//
//  HomeViewController.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"
@interface HomeViewController : UIViewController <UIGestureRecognizerDelegate>
{
    
    IBOutlet HLayoutView *hLayoutView;
    IBOutlet VLayoutView *vLayoutRight;
    IBOutlet VLayoutView *vLayoutLeft;
    
    NSMutableArray *aryTextTemp; // Dummy text for title
    // View which we are adding into minimum height's vLayout
    // It will remove when we add new in it.
    UIView *viewTemp;
    float heightLeftView;
    float heightRightView;
    // End
    
    // Create Detail View Object
    DetailViewController *objDetailViewController;
    // End
    
    NSMutableArray *aryData;
}
-(void)callAlbumListView;
-(void)addViewinLayoutView :(NSMutableDictionary *)dic : (BOOL)isLeft : (int)tagForView;
-(void)setViewOnLowHeightView;
@end
