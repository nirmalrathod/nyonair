//
//  HomeViewController.m
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    // Set Method for Bar Button and left menu
    [self setupMenuBarButtonItems];
    // End
    
    heightLeftView = 0.0;
    heightRightView = 0.0;

    // Set the Layout manager's default property
    hLayoutView.hAlignment = UIControlContentHorizontalAlignmentLeft;
    hLayoutView.vAlignment = UIControlContentVerticalAlignmentTop;
    hLayoutView.topMargin = 10;
    hLayoutView.spacing = 10;
    hLayoutView.bottomMargin = 10;
    
    vLayoutLeft.hAlignment = UIControlContentHorizontalAlignmentLeft;
    vLayoutLeft.vAlignment = UIControlContentVerticalAlignmentTop;
    vLayoutLeft.topMargin = 10;
    vLayoutLeft.spacing = 10;
    vLayoutLeft.bottomMargin = 10;
    
    vLayoutRight.hAlignment = UIControlContentHorizontalAlignmentLeft;
    vLayoutRight.vAlignment = UIControlContentVerticalAlignmentTop;
    vLayoutRight.topMargin = 10;
    vLayoutRight.spacing = 10;
    vLayoutRight.bottomMargin = 10;
    
    vLayoutLeft.showsHorizontalScrollIndicator = FALSE;
    vLayoutLeft.showsVerticalScrollIndicator = FALSE;

    vLayoutRight.showsHorizontalScrollIndicator = FALSE;
    vLayoutRight.showsVerticalScrollIndicator = FALSE;
    
//    vLayoutRight.layer.cornerRadius = 5;
//    vLayoutLeft.layer.cornerRadius = 5;
    // End

    aryData = nil;
    aryData = [[NSMutableArray alloc] init];
    
    appDelegate.isDetailChanged = TRUE;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)staticViewMethod
{
    
    // Static Array
    
    aryTextTemp = [[NSMutableArray alloc] initWithObjects:
                   @"SUPER BOWL XLVIII at METLIFE STADIUM",
                   @"NYC WATERWAYS",
                   @"THE NAVY'S BLUE ANGELS NYC FLY BY",
                   @"AIR TO AIR",
                   @"THE TREE AT ROCKEFELLER CENTER",
                   @"THE SHIPS",
                   @"THE GOVERNORS BALL NYC MUSIC FESTIVAL",
                   @"HUDSON RIVER VALLEY LEAF PEEPING",
                   @"CONSTRUCTION OF THE FREEDOM TOWER",
                   @"COOLING OFF IN NYC",
                   nil];
    
    NSMutableArray *aryLike = [[NSMutableArray alloc] initWithObjects:
                               @"500",
                               @"9",
                               @"0",
                               @"6",
                               @"5000",
                               @"4",
                               @"1",
                               @"23",
                               @"0",
                               @"999",
                               nil];
    
    NSMutableArray *aryView = [[NSMutableArray alloc] initWithObjects:
                               @"1000",
                               @"78",
                               @"3",
                               @"23",
                               @"9000",
                               @"4",
                               @"2",
                               @"86",
                               @"9",
                               @"1000",
                               nil];
    
    
    
    NSMutableArray *aryData = [[NSMutableArray alloc] init];
    for(int i=0 ; i <= 9 ; i++)
    {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:[NSString stringWithFormat:@"img%d.png",i] forKey:@"image"];
        [dic setObject:[aryLike objectAtIndex:i] forKey:@"like"];
        [dic setObject:[aryView objectAtIndex:i] forKey:@"view"];
        [dic setObject:[aryTextTemp objectAtIndex:i] forKey:@"desc"];
        [aryData addObject:dic];
        dic = nil;
    }
    // End
    
    
    // Remove the viewTemp before add any view into vLayoutView
    [viewTemp removeFromSuperview];
    // End
    
    // Add view on both left and right view
    for(int i=0 ; i <= 9 ; i++)
    {
        BOOL isleft = FALSE;
        if(i%2==0)
            isleft = TRUE;
        
        [self addViewinLayoutView:[aryData objectAtIndex:i] :isleft :i];
    }
    // End
    
    [self setViewOnLowHeightView];
    [SVProgressHUD dismiss];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    if(appDelegate.isDetailChanged)
    {
        for(id view in vLayoutLeft.subviews)
            [view removeFromSuperview];

        for(id view in vLayoutRight.subviews)
            [view removeFromSuperview];

        appDelegate.isDetailChanged = FALSE;
        aryData = nil;
        aryData = [[NSMutableArray alloc] init];
        [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(callAlbumListView) userInfo:nil repeats:NO];
    }
    appDelegate.lastController = viewHomeScreen;
    self.title = TITLE_PHOTO;
    self.navigationItem.leftBarButtonItem = nil;
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
    self.navigationItem.leftBarButtonItem = nil;
    [super viewWillDisappear:animated];
}
#pragma mark - ScrollView delegate method
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    vLayoutLeft.contentOffset = sender.contentOffset;
    vLayoutRight.contentOffset = sender.contentOffset;
}
#pragma mark - Set Temp View in LayoutView
-(void)setViewOnLowHeightView
{
    @try
    {
        // Here, we set the maximum height to both the vLayoutView
        
        float diffHeight = 0.0;
        BOOL isLeft;
        if(heightLeftView > heightRightView)
        {
            diffHeight = heightLeftView - heightRightView;
            isLeft = FALSE;
        }
        else
        {
            diffHeight = heightRightView - heightLeftView;
            isLeft = TRUE;
        }
        
        if(diffHeight > 0.0)
        {
            [viewTemp removeFromSuperview];
            viewTemp = nil;
            viewTemp = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 145, diffHeight)];
            if(isLeft)
                [vLayoutLeft addSubview:viewTemp];
            else
                [vLayoutRight addSubview:viewTemp];
        }
        // End
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

#pragma mark - Add View into LayoutView
-(void)addViewinLayoutView :(NSMutableDictionary *)dic : (BOOL)isLeft : (int)tagForView
{
    @try
    {
        
    
        UIView *viewInner = [[UIView alloc] init];
        
        
        NSString *strImageName;
        NSArray *ary = [[dic valueForKey:KEY_ALBUMCOVERIMAGE] componentsSeparatedByString:@"/"];
        strImageName = [ary lastObject];
        strImageName = [strImageName stringByReplacingOccurrencesOfString:@" " withString:@""];

        NSString *imagePath = [CACHE_DIRECTORY stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",strImageName]];
        UIImage *img = [UIImage imageWithContentsOfFile:imagePath];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
        
        UIFont *cellFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
        float heighttext = [StringValidation heightFromString:[dic objectForKey:KEY_ALBUMNAME] :cellFont];
        
        UILabel  *lblName = [[UILabel alloc] initWithFrame:CGRectMake(5, imgView.frame.size.height + 3, 135, heighttext)];
        lblName.font = cellFont;
        lblName.numberOfLines = 0;
        lblName.text = [dic objectForKey:KEY_ALBUMNAME];
        lblName.textColor = [UIColor colorWithRed:0.0/255.0 green:50.0/255.0 blue:108.0/255.0 alpha:1.0];
        lblName.backgroundColor = [UIColor clearColor];
        
        
        
        
        imgView.frame = CGRectMake(0, 0, 145, imgView.frame.size.height);
        
        UIView *viewViewer = [[UIView alloc] initWithFrame:CGRectMake(0, imgView.frame.size.height + lblName.frame.size.height + 5, 145, 30)];
        [viewViewer setBackgroundColor:[UIColor clearColor]];
        
        viewInner.frame = CGRectMake(0, 2, 145, imgView.frame.size.height + lblName.frame.size.height + viewViewer.frame.size.height + 5);
        
        UIImageView *imgViewer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photoView.png"]];
        imgViewer.frame = CGRectMake(5, 3, 15, 15);
        
        UILabel *lblCount = [[UILabel alloc] initWithFrame:CGRectMake(imgViewer.frame.size.width + imgViewer.frame.origin.x + 5, 3, 24, 15)];
        lblCount.text = [appDelegate convertIntoKserias:[[dic valueForKey:KEY_ALBUMVIEWCOUNT] intValue]];
        lblCount.font = cellFont;
        lblCount.textColor = [UIColor colorWithRed:0.0/255.0 green:50.0/255.0 blue:108.0/255.0 alpha:1.0];
        [lblCount setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *imgLike = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photoLike.png"]];
        imgLike.frame = CGRectMake(lblCount.frame.size.width + lblCount.frame.origin.x + 5, 3, 15, 15);
        
        UILabel *lblLikeCount = [[UILabel alloc] initWithFrame:CGRectMake(imgLike.frame.size.width + imgLike.frame.origin.x + 5, 3, 24, 15)];
        lblLikeCount.font = cellFont;
        lblLikeCount.text = [appDelegate convertIntoKserias:[[dic valueForKey:KEY_ALBUMFAVORITECOUNT] intValue]];
        lblLikeCount.textColor = [UIColor colorWithRed:0.0/255.0 green:50.0/255.0 blue:108.0/255.0 alpha:1.0];
        
                [lblLikeCount setBackgroundColor:[UIColor clearColor]];
        
        [viewViewer addSubview:imgViewer];
        [viewViewer addSubview:lblCount];
        [viewViewer addSubview:imgLike];
        [viewViewer addSubview:lblLikeCount];
        
        
        [viewInner setBackgroundColor:[UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0]];
        
        viewInner.layer.cornerRadius = 3;
        viewInner.layer.masksToBounds = YES;
        
        
        [viewInner addSubview:imgView];
        [viewInner addSubview:lblName];
        [viewInner addSubview:viewViewer];
        
        //    viewInner.layer.cornerRadius = 4;
        //    viewInner.layer.borderColor = [UIColor greenColor].CGColor;
        //    viewInner.layer.borderWidth = 2;
        
        viewInner.tag = tagForView;
        UITapGestureRecognizer *messagesTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(detailViewTapped:)];
        [messagesTap setDelegate:self];
        [messagesTap setNumberOfTapsRequired:1];
        [viewInner addGestureRecognizer:messagesTap];
        
        img = nil;
        imgView = nil;
        lblName = nil;
        viewViewer = nil;
        imgViewer = nil;
        lblCount = nil;
        imgLike = nil;
        lblLikeCount = nil;
        
        if(isLeft)
        {
            heightLeftView = heightLeftView + viewInner.frame.size.height;
            [vLayoutLeft addSubview:viewInner];
        }
        else
        {
            heightRightView = heightRightView + viewInner.frame.size.height;
            [vLayoutRight addSubview:viewInner];
        }
        
        
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

#pragma mark - Tap Gesture On View
- (void)detailViewTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    @try
    {
        int myViewTag = gestureRecognizer.view.tag;
        
        objDetailViewController = nil;
        if(appDelegate.isiPhone5)
            objDetailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
        else
            objDetailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPhone4" bundle:nil];
        
        NSLog(@"ALBUMID : %@",[NSString stringWithFormat:@"%@",[[aryData objectAtIndex:myViewTag] valueForKey:KEY_ALBUMID]]);
        objDetailViewController.strAlbumId = [NSString stringWithFormat:@"%@",[[aryData objectAtIndex:myViewTag] valueForKey:KEY_ALBUMID]];
        objDetailViewController.strTitle = [NSString stringWithFormat:@"%@",[[aryData objectAtIndex:myViewTag] valueForKey:KEY_ALBUMNAME]];
        [self.navigationController pushViewController:objDetailViewController animated:YES];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems {
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
        self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    } else {
        self.navigationItem.leftBarButtonItem = nil;//[self leftMenuBarButtonItem];
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:IMAGE_MENU style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(leftSideMenuButtonPressed:)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 22.0f, 15.0f)];
    [a1 addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_MENU forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

- (UIBarButtonItem *)backBarButtonItem {
    
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 20.0, 20.0f)];
    [a1 setImageEdgeInsets:UIEdgeInsetsMake(0, -9, 0, 0)];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
        a1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [a1 addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_BACK forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}

- (void)rightSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}
#pragma mark - Call Album List View Service
-(void)callAlbumListView
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
    
    NSMutableDictionary *dicParmeter = [[NSMutableDictionary alloc] init];
    [dicParmeter setValue:@"0" forKey:KEY_OFFSET];
    [dicParmeter setValue:appDelegate.strCustomerID forKey:KEY_CUSTOMERID];
    
    [WebAPIRequest callWebSevice:self webServiceName:WS_ALBUMSLIST parameterDic:dicParmeter];
    dicParmeter = nil;

}
#pragma mark - Web-Service Response Method
-(void)uploadRequestFinished:(ASIHTTPRequest *)request
{
    @try {
        [appDelegate endIgnoreEvent];
        [SVProgressHUD dismiss];
        NSString *responseString = [request responseString];
        
        NSDictionary *returnDict;
        if([responseString length])
        {
            returnDict = [responseString JSONValue];
        }
        
        if(returnDict)
        {
            if([[returnDict valueForKey:KEY_SUCCESS] intValue] == 0)
            {
                [SVProgressHUD showErrorWithStatus:[returnDict valueForKey:KEY_MSG]];
            }
            else
            {
                [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
                if(aryData.count == 0)
                {
                    aryData = [[returnDict valueForKey:KEY_DATA] mutableCopy];
                }
                else
                {
                    [aryData addObjectsFromArray:[returnDict valueForKey:KEY_DATA]];
                }
                
                // Remove the viewTemp before add any view into vLayoutView
                [viewTemp removeFromSuperview];
                // End
                
                
                // Add view on both left and right view
                for(int i=0 ; i < aryData.count ; i++)
                {
                    [appDelegate storedImage:[[aryData objectAtIndex:i] valueForKey:KEY_ALBUMCOVERIMAGE]];
                    BOOL isleft = FALSE;
                    if(i%2==0)
                        isleft = TRUE;
                    
                    [self addViewinLayoutView:[aryData objectAtIndex:i] :isleft :i];
                }
                // End
                
                [self setViewOnLowHeightView];
                [SVProgressHUD dismiss];
                
            }
        }
        
        
        responseString = nil;
        returnDict = nil;
        
    }
    @catch (NSException *exception) {
        NSLog(@"%s ==> %@",__FUNCTION__,exception.description);
    }
}
- (void)uploadRequestFailed:(ASIHTTPRequest *)request
{
    [appDelegate endIgnoreEvent];
    [SVProgressHUD showErrorWithStatus:MSG_SERVERERROR];
}

#pragma mark - Other Memory Method
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
