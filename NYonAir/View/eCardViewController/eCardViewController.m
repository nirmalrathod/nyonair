//
//  eCardViewController.m
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import "eCardViewController.h"
#import "HomeViewController.h"
@interface eCardViewController ()

@end

@implementation eCardViewController

#define imgStep1  [UIImage imageNamed:@"step1.png"]
#define imgStep2  [UIImage imageNamed:@"step2.png"]
#define imgStep3  [UIImage imageNamed:@"step3.png"]

#define fontSizeForMessage 15.0
#define fontSizeForPreview 40.0
@synthesize carousel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
//    NSURL *whatsappURL = [NSURL URLWithString:@"whatsapp://send?text=Hello%2C%20World!"];
//    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
//        [[UIApplication sharedApplication] openURL: whatsappURL];
//    }
//    return;
    // Set Method for Bar Button and left menu
    [self setupMenuBarButtonItems];
    // End
    
    btnPainBrush.layer.cornerRadius = 5.0;
    btnPainBrush.layer.masksToBounds = YES;
    btnPainBrush.backgroundColor = [UIColor orangeColor];
    btnPainBrush.layer.borderWidth = 1.0;
    btnPainBrush.layer.borderColor = [UIColor blackColor].CGColor;

    rectView = viewMessage.frame;
    isEditView = FALSE;
    
    //viewMessage.hidden = TRUE;
    viewPreview.hidden = TRUE;
    
    viewEmailAddress.hidden = TRUE;
    
    // Set Carousel view and its properity
    selectViewIndex = 0;
    carousel.type = iCarouselTypeLinear;
    carousel.scrollEnabled = FALSE;
    carousel.bounces = FALSE;
    [carousel reloadData];
    // End


    CGRect rect =CGRectMake((viewMessage.frame.size.width/2)-100, (viewMessage.frame.size.height/2)-20, 200, 40);//320-10
    CGRect rectText  =CGRectMake(10, 3, rect.size.width - 10, 40);
    
    containerView = [[UIView alloc] initWithFrame:rect];
    
	textView = [[HPGrowingTextView alloc] initWithFrame:rectText];
    textView.isScrollable = NO;
    textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
	textView.minNumberOfLines = 1;
	textView.maxNumberOfLines = 10;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
	textView.returnKeyType = UIReturnKeyDone; //just as an example
    textView.font = [UIFont fontWithName:FONTNAMEBOLD size:15.0];
    btnBold.selected = TRUE;
	textView.delegate = self;
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor clearColor];
    textView.placeholder = @"";//@"Enter your commnet";
    
    textView.textColor = [UIColor orangeColor];
    
    UIImage *rawEntryBackground = [UIImage imageNamed:@"MessageEntryInputField.png"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.frame = CGRectMake(5, 0, 248, 40);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = [UIImage imageNamed:@"MessageEntryBackground.png"];
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    
    [containerView addSubview:imageView];
    [containerView addSubview:textView];
    [containerView addSubview:entryImageView];
    
    containerView.hidden = TRUE;
    [viewMessage addSubview:containerView];
    
//    [containerView setBackgroundColor:[UIColor redColor]];
//    textView.backgroundColor = [UIColor grayColor];

    UISwipeGestureRecognizer *swipeGestureLeftObjectViewVRM = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft)] ;
    swipeGestureLeftObjectViewVRM.numberOfTouchesRequired = 1;
    swipeGestureLeftObjectViewVRM.direction = (UISwipeGestureRecognizerDirectionLeft);
    [carousel addGestureRecognizer:swipeGestureLeftObjectViewVRM];
    
    UISwipeGestureRecognizer *swipeGestureRightObjectViewVRM = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)] ;
    swipeGestureRightObjectViewVRM.numberOfTouchesRequired = 1;
    swipeGestureRightObjectViewVRM.direction = (UISwipeGestureRecognizerDirectionRight);
    [carousel addGestureRecognizer:swipeGestureRightObjectViewVRM];
    

    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textFieldTapped:)];
    [viewMessage addGestureRecognizer:tapGesture];
    
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(textFieldDragged:)];
	[containerView addGestureRecognizer:gesture];

    
    imgSteps.image = imgStep1;
    lblMessage.text = MSG_SELECTTEMPLATE;
    viewMessage.layer.cornerRadius = 5.0;
    viewMessage.layer.borderWidth = 3.0;
    viewMessage.layer.borderColor = [UIColor grayColor].CGColor;
    viewMessage.layer.masksToBounds = YES;

    rectViewMessage = viewMessage.frame;
    
    viewPreview.layer.cornerRadius = 5.0;
    viewPreview.layer.borderWidth = 3.0;
    viewPreview.layer.borderColor = [UIColor grayColor].CGColor;
    viewPreview.layer.masksToBounds = YES;


    if(!appDelegate.isiPhone5)
        viewPreview.frame = CGRectMake(25, 185, 270, 203);
    else
            carousel.frame =CGRectMake(25, 165, 270, 203);
    
    viewPreview.hidden = TRUE;
    holdingIndex = 1;
    
    btnLeft.selected = TRUE;
    btnCenter.selected = FALSE;
    btnRight.selected = FALSE;
    

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    appDelegate.lastController = viewECard;
    self.title = TITLE_ECARD;
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
-(void)swipeRight
{
    if(carousel.currentItemIndex > 0)
    {
        [carousel scrollToItemAtIndex:carousel.currentItemIndex-1 animated:YES];
    }
}
-(void)swipeLeft
{
    if(carousel.currentItemIndex != 10)
    {
        [carousel scrollToItemAtIndex:carousel.currentItemIndex+1 animated:YES];
        
    }
}

#pragma mark - Gesture Method
- (void)textFieldTapped:(UITapGestureRecognizer *)gesture
{
    @try
    {
        CGPoint translatedPoint = [gesture locationInView: viewMessage];

        
        if(containerView.hidden == FALSE)
        {
            if (CGRectContainsPoint(containerView.frame, translatedPoint))
            {
                
            }
            else
            {
                return;

            }

        }
        
//        CGPoint translatedPoint = [(UITapGestureRecognizer*)gesture translationInView:viewMessage];
        
        if([(UITapGestureRecognizer*)gesture state] == UIGestureRecognizerStateBegan) {
            
            firstX = [[gesture view] center].x;
            firstY = [[gesture view] center].y;
        }

        int ff = firstX+translatedPoint.x;
        if(ff < containerView.frame.size.width/2)
            ff = containerView.frame.size.width/2;
        translatedPoint = CGPointMake(ff, firstY+translatedPoint.y);
        
        [containerView setCenter:translatedPoint];
        
     /*   if([(UIPanGestureRecognizer*)gesture state] == UIGestureRecognizerStateEnded) {
            
            CGFloat finalX = translatedPoint.x + (.35*[(UIPanGestureRecognizer*)gesture velocityInView:viewMessage].x);
            CGFloat finalY = translatedPoint.y + (.35*[(UIPanGestureRecognizer*)gesture velocityInView:viewMessage].y);
            
            if(finalX < 0) {
                
                finalX = (containerView.frame.size.width/2)+2;
            }
            
            else if(finalX > viewMessage.frame.size.width) {
                
                finalX = viewMessage.frame.size.width-110;
            }
            
            if(finalY < 25) {
                
                finalY = 25;
            }
            
            else if(finalY > viewMessage.frame.size.height) {
                
                finalY = viewMessage.frame.size.height- containerView.frame.size.height;
            }
            
            [containerView setCenter:CGPointMake(finalX, finalY)];
      
        }
        */
        containerView.hidden = FALSE;
        [textView becomeFirstResponder];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s --> %@",__FUNCTION__,exception.description);
    }
    
}
/*
- (void)textFieldDragged:(UIPanGestureRecognizer *)gesture
{
    @try
    {
        
        CGPoint translatedPoint = [(UIPanGestureRecognizer*)gesture translationInView:viewMessage];
        
        if([(UIPanGestureRecognizer*)gesture state] == UIGestureRecognizerStateBegan) {
            
            firstX = [[gesture view] center].x;
            firstY = [[gesture view] center].y;
        }
        
        translatedPoint = CGPointMake(firstX+translatedPoint.x, firstY+translatedPoint.y);
        
        [[gesture view] setCenter:translatedPoint];
        
        if([(UIPanGestureRecognizer*)gesture state] == UIGestureRecognizerStateEnded) {
            
            CGFloat finalX = translatedPoint.x + (.35*[(UIPanGestureRecognizer*)gesture velocityInView:viewMessage].x);
            CGFloat finalY = translatedPoint.y + (.35*[(UIPanGestureRecognizer*)gesture velocityInView:viewMessage].y);
            
            if(finalX < (containerView.frame.size.width/2)) {
                
                finalX = (containerView.frame.size.width/2)-3;
            }
            
            else if(finalX > (viewMessage.frame.size.width/2)) {
//            else if(finalX +(containerView.frame.size.width/2) > viewMessage.frame.size.width)
//                    {
            
                finalX = viewMessage.frame.size.width-textView.frame.size.width - 10;//viewMessage.frame.size.width-107;
//                        finalX = viewMessage.frame.size.width - textView.frame.size.width ;
//                        finalX + = containerView.frame.origin.x
//                        finalX += 40;
            }
            
            if(finalY < 25) {
                
                finalY = 25;
            }
            
            else if(finalY + 10 > viewMessage.frame.size.height) {
                
                finalY = viewMessage.frame.size.height - (containerView.frame.size.height-13);
            }
            
            [[gesture view] setCenter:CGPointMake(finalX, finalY)];
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
} */

- (void)textFieldDragged:(UIPanGestureRecognizer *)gesture
{
    @try
    {
        
        CGPoint translatedPoint = [(UIPanGestureRecognizer*)gesture translationInView:viewMessage];
        
        if([(UIPanGestureRecognizer*)gesture state] == UIGestureRecognizerStateBegan) {
            
            firstX = [[gesture view] center].x;
            firstY = [[gesture view] center].y;
        }
        
        translatedPoint = CGPointMake(firstX+translatedPoint.x, firstY+translatedPoint.y);
        
        [[gesture view] setCenter:translatedPoint];
        
        if([(UIPanGestureRecognizer*)gesture state] == UIGestureRecognizerStateEnded) {
            
            CGFloat finalX = translatedPoint.x + (.35*[(UIPanGestureRecognizer*)gesture velocityInView:viewMessage].x);
            CGFloat finalY = translatedPoint.y + (.35*[(UIPanGestureRecognizer*)gesture velocityInView:viewMessage].y);
            
            if(finalX < (containerView.frame.size.width/2)) {
                
                finalX = (containerView.frame.size.width/2)-3;
            }
            
            else if(finalX > (viewMessage.frame.size.width/2)) {
                
                finalX = viewMessage.frame.size.width-107;
            }
            
            if(finalY < 25) {
                
                finalY = 25;
            }
            
            else if(finalY + 10 > viewMessage.frame.size.height) {
                
                finalY = viewMessage.frame.size.height - (containerView.frame.size.height-13);
            }
            
            [[gesture view] setCenter:CGPointMake(finalX, finalY)];
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

#pragma mark - Add & Remove Notification
-(void)addObserverIntoView
{
    return;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}
-(void)removeObserverFromView
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - HPGrowingText Method

- (BOOL)growingTextViewShouldBeginEditing:(HPGrowingTextView *)growingTextView
{
    @try
    {
        [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                         animations:^{
                             
                             float yPosition;
                             if(appDelegate.isiPhone5)
                                 yPosition = -90;
                             else
                                 yPosition = -80;
                             
                             //                         viewMessage.frame = CGRectMake(0, yPosition, viewMessage.frame.size.width, viewMessage.frame.size.height);
                             //                             viewMessage.frame = CGRectMake(rectViewMessage.origin.x, rectViewMessage.origin.y - yPosition, rectViewMessage.size.width, rectViewMessage.size.height);
                             
                             viewAllItems.frame = CGRectMake(0, yPosition, viewAllItems.frame.size.width, viewAllItems.frame.size.height);
                             
                         }
                         completion:^(BOOL finished){
                         }];
        
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
    return YES;
}
/*
- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    @try
    {
        if([text isEqualToString:@"\n"])
            [textView resignFirstResponder];
        
        
        
        NSString *newString = [growingTextView.text stringByReplacingCharactersInRange:range withString:text];
        CGFloat width =  [newString sizeWithFont:growingTextView.font].width;
        CGRect rectTextFieldView = containerView.frame;
        
        if(newString.length == 0)
        {
            CGRect rect =CGRectMake((viewMessage.frame.size.width/2)-100, (viewMessage.frame.size.height/2)-20, 20, 40);//320-10
            CGRect rectText  =CGRectMake(10, 3, rect.size.width - 10, 40);
            containerView.frame = rectText;
        }

        
        
        if(rectTextFieldView.size.width < 200)
        {
            CGRect rectTextField = textView.frame;
            rectTextField.size.height = rectTextFieldView.size.height;
            rectTextField.size.width = rectTextFieldView.size.width;
            
            //        textView.frame = rectTextField;
            rectTextFieldView.size.width = width + 30;
            containerView.frame = rectTextFieldView;
        }
        
        return YES;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Execption : %@",exception.description);
    }
}
*/
- (BOOL)growingTextViewShouldEndEditing:(HPGrowingTextView *)growingTextView
{
    @try
    {
        [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                         animations:^{
                             //                         self.view.frame = rectView;
                             float yPosition = viewAllItems.frame.origin.y;
                             if(appDelegate.isiPhone5)
                                 yPosition = yPosition - 90;
                             else
                                 yPosition = -80;
                             //                         self.view.frame = CGRectMake(0, yPosition, self.view.frame.size.width, self.view.frame.size.height);
                             //                             viewMessage.frame = rectViewMessage;
                             viewAllItems.frame = CGRectMake(0, 0, viewAllItems.frame.size.width, viewAllItems.frame.size.height);
                             
                             
                             [growingTextView resignFirstResponder];
                             
                             
                         }
                         completion:^(BOOL finished){
                             
                             NSString *strLabelText = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                             if(strLabelText.length == 0)
                                 [containerView setHidden:TRUE];

                         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
    return YES;
}


#pragma mark - Notification Method
- (void)keyboardWasShown:(NSNotification *)notification
{
    @try
    {
        [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                         animations:^{
                             
                             float yPosition;
                             if(appDelegate.isiPhone5)
                                 yPosition = -90;
                             else
                                 yPosition = -80;
                             
                             //                         viewMessage.frame = CGRectMake(0, yPosition, viewMessage.frame.size.width, viewMessage.frame.size.height);
//                             viewMessage.frame = CGRectMake(rectViewMessage.origin.x, rectViewMessage.origin.y - yPosition, rectViewMessage.size.width, rectViewMessage.size.height);
                             
                            viewAllItems.frame = CGRectMake(0, yPosition, viewAllItems.frame.size.width, viewAllItems.frame.size.height);
                             
                         }
                         completion:^(BOOL finished){
                         }];
        
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

-(void)keyboardWillHide:(NSNotification *)aNotification
{
    @try
    {
        [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                         animations:^{
                             //                         self.view.frame = rectView;
                             float yPosition = viewAllItems.frame.origin.y;
                             if(appDelegate.isiPhone5)
                                 yPosition = yPosition - 90;
                             else
                                 yPosition = -80;
                             //                         self.view.frame = CGRectMake(0, yPosition, self.view.frame.size.width, self.view.frame.size.height);
//                             viewMessage.frame = rectViewMessage;
                             viewAllItems.frame = CGRectMake(0, 0, viewAllItems.frame.size.width, viewAllItems.frame.size.height);
                             
                             
                             
                             
                         }
                         completion:^(BOOL finished){
                         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}


#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return 10;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    @try
    {
        
        
        CGRect viewFrame = CGRectMake(0, 0, 270, 203);
        if(appDelegate.isiPhone5)
        {
            viewFrame.size.height = carousel.frame.size.height;
            viewFrame.size.width = carousel.frame.size.width;
        }
        //        imgView.center = CGPointMake(viewFrame.size.width/2, viewFrame.size.height/2);
        view = [[UIView alloc] initWithFrame:viewFrame];
        [view setBackgroundColor:[UIColor clearColor]];
        view.contentMode = UIViewContentModeScaleToFill;
        
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"eCard%d.jpg",(int)index]]];
        
        imgView.frame = viewFrame;
        [view addSubview:imgView];
        
        if(index == selectViewIndex)
        {
            view.layer.borderColor = [UIColor colorWithRed:242.0/255.0 green:98.0/255.0 blue:88.0/255.0 alpha:1.0].CGColor;
            view.layer.borderWidth = 5.0;
            imgViewMessage.image = imgView.image;
            imgPreview.image = imgView.image;
            
        }
        else
        {
            view.layer.borderColor = [UIColor clearColor].CGColor;
            view.layer.borderWidth = 0.0;
        }
        
        imgView.layer.cornerRadius = 5.0;
        view.layer.cornerRadius = 5.0;
        view.layer.masksToBounds = YES;
        imgView.layer.masksToBounds = YES;
        
        
        return view;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}


- (NSUInteger)numberOfPlaceholdersInCarousel:(iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 2;
}

- (UIView *)carousel:(iCarousel *)carousel placeholderViewAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    return nil;
}

- (CATransform3D)carousel:(iCarousel *)_carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * carousel.itemWidth);
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.01f;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
 
    selectViewIndex = (int)index;
    [carousel reloadData];
}

#pragma mark - TextField Delegate Method
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.25f
                     animations:^{
                         
                         if(textField == txtEmail)
                         {
                             float yPosition = 0.0;
                             if(appDelegate.isiPhone5)
                                 yPosition = rectViewEmailAddress.origin.y - 30;
                             else
                                 yPosition = rectViewEmailAddress.origin.y - 70;
                             
                         viewEmailAddress.frame = CGRectMake(rectViewEmailAddress.origin.x,yPosition , rectViewEmailAddress.size.width, rectViewEmailAddress.size.height);
                         }
                         else
                         {
                             NSLog(@"%@ %@",textField, textField.superview);
                         }
                     }];

    return TRUE;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.25f
                     animations:^{
                         if(textField == txtEmail)
                         {
                             viewEmailAddress.frame = rectViewEmailAddress;
                         }
                         else
                         {
                             
                         }
                     }];
    
    [textField resignFirstResponder];
}
#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems {
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
        self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    } else {
        self.navigationItem.leftBarButtonItem = nil;//[self leftMenuBarButtonItem];
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:IMAGE_MENU style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(leftSideMenuButtonPressed:)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 22.0f, 15.0f)];
    [a1 addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_MENU forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

- (UIBarButtonItem *)backBarButtonItem {
    
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 20.0, 20.0f)];
    [a1 setImageEdgeInsets:UIEdgeInsetsMake(0, -9, 0, 0)];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
        a1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [a1 addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_BACK forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}

- (void)rightSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}
#pragma mark - Other Memory Method
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Take Screen Shot
-(UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)newSize {
    
    float width = newSize.width;
    float height = newSize.height;
    
    UIGraphicsBeginImageContext(newSize);
    CGRect rect = CGRectMake(0, 0, width, height);
    
    float widthRatio = image.size.width / width;
    float heightRatio = image.size.height / height;
    float divisor = widthRatio > heightRatio ? widthRatio : heightRatio;
    
    width = image.size.width / divisor;
    height = image.size.height / divisor;
    
    rect.size.width  = width;
    rect.size.height = height;
    
    if(height < width)
        rect.origin.y = height / 3;
    [image drawInRect: rect];
    
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return smallImage;
    
}

-(UIImage*)screenShotOf:(UIView*)view atScale:(CGFloat)scale
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, scale);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}
-(CGRect)convertPointYY
{
    CGPoint point = containerView.frame.origin;//CGPointMake(9, 9);
    NSValue *pointObj = [NSValue valueWithCGPoint:point];
    
    // CGSize converted to NSValue
    CGSize size = CGSizeMake(200, 80);
    NSValue *sizeObj = [NSValue valueWithCGSize:size];
    
    // CGRect from CGPoint and CGSize converted to NSValue
    CGRect rect = CGRectMake(point.x, point.y, size.width, size.height);
    NSValue *rectObj = [NSValue valueWithCGRect:rect];
    
    // Add the objects to a collection
    NSArray *array = [NSArray arrayWithObjects:pointObj, sizeObj, rectObj, nil];
    
    // Print to console the objects in the collection
    NSLog(@"array content: %@", array);
    
    // Restore from NSValue to C structures
    CGPoint pointRestored = [pointObj CGPointValue];
    CGSize sizeRestored = [sizeObj CGSizeValue];
    CGRect rectRestored = [rectObj CGRectValue];
    
    // Print restored values
    NSLog(@"point x:%f y:%f", pointRestored.x, pointRestored.y);
    NSLog(@"size  w:%f h:%f", sizeRestored.width, sizeRestored.height);
//    NSLog(@"rect x:%f y:%f w:%f h:%f", rectRestored.origin.x, rectRestored.origin.y,
    
    return rectRestored;
}
-(CGRect)scaleView :(CGSize)targetSize
{
    UIView *sourceImage = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 270, 203)];
    UIView *newImage = nil;
    
    CGSize imageSize = sourceImage.frame.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor < heightFactor)
            scaleFactor = widthFactor;
        else
            scaleFactor = heightFactor;
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
    }
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    return thumbnailRect;
    
}
-(UIImage *)takeScreenShote
{
    @try
    {
//        CGRect rect = [viewMessage bounds];
//        UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
//        CGContextRef context = UIGraphicsGetCurrentContext();
//        [viewMessage.layer renderInContext:context];
//        UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        
//        return capturedImage;

        
        
        UIView *view = [[UIView alloc] init];
        UIImageView *imgTemp = [[UIImageView alloc] initWithImage:imgViewMessage.image];
        imgTemp.contentMode = UIViewContentModeScaleToFill;

        view.frame = imgTemp.frame;
        
//        CGPoint originalPoint = [view convertPoint:CGPointZero fromView:viewMessage];
        CGPoint originalPoint = [containerView convertPoint:containerView.frame.origin toView:view];
//
//        CGRect  originalFrame = [[textView superview] convertRect: textView.frame toView: view];
//        originalFrame.origin = originalPoint;
        
        CGRect f = [view convertRect:containerView.frame fromView:viewMessage];

        CGPoint pointInViewCoords = [containerView convertPoint:containerView.frame.origin fromView:view];
        CGPoint originInSuperview = [[containerView superview] convertPoint:CGPointZero fromView:containerView];
        
        CGRect newPoint = [containerView convertRect:f toView:view];
//        f.origin = pointInViewCoords;
//        CGRect bframe = [view convertRect:containerView.frame fromView:containerView.superview];
//        CGPoint aPosViewA = [view convertPoint:CGPointZero fromView:containerView];
        CGRect newFrame = [view convertRect:CGRectZero fromView:containerView];
//        newFrame.origin = originInSuperview;
//        newFrame.size = containerView.frame.size;
   
        
        
        CGPoint point = containerView.frame.origin;
        point.x *= containerView.frame.size.width / imgTemp.frame.size.width;
        point.y *= containerView.frame.size.height /imgTemp.frame.size.height;
        
        newPoint.origin = point;
        
        CGRect frame ;//= [viewMessage convertRect:containerView.frame toView:view];
        frame.origin = point;
        
        CGSize target = CGSizeMake(800, 600);
        frame.size = target;
    
        
        f.origin.y = f.origin.y - 170;
        f.origin.x = f.origin.x + 70;
        HPGrowingTextView *tt = [[HPGrowingTextView alloc] initWithFrame:f];

        tt.backgroundColor = [UIColor clearColor];
        if([[dicEditData valueForKey:TAG_BOLD] intValue] == 0)
            tt.font = [UIFont fontWithName:FONTNAMEBOLD size:fontSizeForPreview];
        else
            tt.font = [UIFont fontWithName:FONTNAME size:fontSizeForPreview];
        
        tt.text = [dicEditData valueForKey:TAG_TEXT];
        
        tt.textColor = [dicEditData objectForKey:TAG_TEXT_COLOR];
        
        if([[dicEditData valueForKey:TAG_ALIGNMENT] isEqualToString:TEXTALLIGNMENT_CENTER])
            tt.textAlignment = NSTextAlignmentCenter;
        else if([[dicEditData valueForKey:TAG_ALIGNMENT] isEqualToString:TEXTALLIGNMENT_LEFT])
            tt.textAlignment = NSTextAlignmentLeft;
        else
            tt.textAlignment = NSTextAlignmentRight;

        //       tt.frame = originalFrame;
        [view addSubview:imgTemp];
       [view addSubview:tt];
        
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 1.0);
        [view.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();

        
        return img;
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}
#pragma mark - Custome Edit View Method

- (IBAction)btnBoldNormalClicked:(id)sender
{
    @try
    {
        UIButton *btn = (id)sender;
        if(btn.selected)
        {
            textView.font = [UIFont fontWithName:FONTNAME size:15.0];
        }
        else
        {
            textView.font = [UIFont fontWithName:FONTNAMEBOLD size:15.0];
        }
        [textView setNeedsDisplay];
        btn.selected = !btn.selected;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnPaintBrushClicked:(id)sender
{
    @try
    {
        UIButton *btn = (id)sender;
        if (!self.wePopoverController) {
            
            ColorViewController *contentViewController = [[ColorViewController alloc] init];
            contentViewController.delegate = self;
            self.wePopoverController = [[WEPopoverController alloc] initWithContentViewController:contentViewController];
            self.wePopoverController.delegate = self;
            self.wePopoverController.passthroughViews = [NSArray arrayWithObject:self.navigationController.navigationBar];
            
            [self.wePopoverController presentPopoverFromRect:btn.frame
                                                      inView:viewEdit
                                    permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionAny)
                                                    animated:YES];
            
        } else {
            [self.wePopoverController dismissPopoverAnimated:YES];
            self.wePopoverController = nil;
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnCenterClicked:(id)sender
{
    @try
    {
        btnCenter.selected = TRUE;
        btnLeft.selected = FALSE;
        btnRight.selected = FALSE;
        textView.textAlignment = NSTextAlignmentCenter;
        [textView setNeedsDisplay];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnLeftAlignmentClicked:(id)sender
{
    @try
    {
        btnLeft.selected = TRUE;
        btnCenter.selected = FALSE;
        btnRight.selected = FALSE;

        textView.textAlignment = NSTextAlignmentLeft;
        [textView setNeedsDisplay];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnRightAlignmentClicked:(id)sender
{
    @try
    {
        btnRight.selected = TRUE;
        btnCenter.selected = FALSE;
        btnLeft.selected = FALSE;

        
        textView.textAlignment = NSTextAlignmentRight;
        [textView setNeedsDisplay];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnDoneClicked:(id)sender
{
    @try
    {
//        [textView resignFirstResponder];
        NSString *strText = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if(strText.length == 0)
        {
            [[[UIAlertView alloc] initWithTitle:MSG_TITLE_NO_TEXT message:MSG_TITLE_TEXT delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
            return;
        }
        


        // Store the new value into Dictionary
        dicEditData = nil;
        dicEditData = [[NSMutableDictionary alloc] init];
        if(btnBold.selected)
            [dicEditData setValue:@"0" forKey:TAG_BOLD];
        else
            [dicEditData setValue:@"1" forKey:TAG_BOLD];
        
        if(btnCenter.selected)
            [dicEditData setValue:TEXTALLIGNMENT_CENTER forKey:TAG_ALIGNMENT];
        else if(btnLeft.selected)
            [dicEditData setValue:TEXTALLIGNMENT_LEFT forKey:TAG_ALIGNMENT];
        else
            [dicEditData setValue:TEXTALLIGNMENT_RIGHT forKey:TAG_ALIGNMENT];
        
        [dicEditData setObject:textView.textColor forKey:TAG_TEXT_COLOR];
        [dicEditData setObject:textView.text forKey:TAG_TEXT];
        // End
        
//        imgPreview.image = [self takeScreenShote];
        
        ImageEditor *imageEditorView = [[ImageEditor alloc] initWithFrame:viewMessage.frame];
        imageEditorView.image = imgPreview.image;
        
        CGRect frameLabel = containerView.frame;
        frameLabel.origin.x += 10;
//        frameLabel.origin.y += 20;
        
        UILabel *lbl = [[UILabel alloc] initWithFrame:frameLabel];

        lbl.backgroundColor = [UIColor clearColor];
        lbl.font = textView.font;
        lbl.text = textView.text;
        lbl.textColor = textView.textColor;
        lbl.textAlignment = textView.textAlignment;
        [imageEditorView addSubview:lbl];

        imgPreview.image = [imageEditorView getOriginalImage];
        [viewEdit slideOutTo:kFTAnimationBottom duration:ANIMATION_TIME delegate:self];
//        [NSTimer scheduledTimerWithTimeInterval:ANIMATION_TIME target:self selector:@selector(resignTextField) userInfo:nil repeats:NO];

    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}
-(void)resignTextField
{
    [textView resignFirstResponder];
}
- (IBAction)btnCancelClicked:(id)sender
{
    @try
    {
        [textView resignFirstResponder];
        [viewEdit slideOutTo:kFTAnimationBottom duration:ANIMATION_TIME delegate:self];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}
-(void)setValueOnView
{
    @try
    {
        if(dicEditData)
        {
            if([[dicEditData valueForKey:TAG_BOLD] intValue] == 0)
                textView.font = [UIFont fontWithName:FONTNAMEBOLD size:fontSizeForMessage];
            else
                textView.font = [UIFont fontWithName:FONTNAME size:fontSizeForMessage];
            
            
            NSString *strLabelText = [[dicEditData valueForKey:TAG_TEXT] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

            
            textView.text = strLabelText;
            
            if([[dicEditData valueForKey:TAG_TEXT] length] == 0)
            {
                containerView.hidden = TRUE;
            }
            else
            {
                containerView.hidden = FALSE;
            }
            
            textView.textColor = [dicEditData objectForKey:TAG_TEXT_COLOR];
            
            if([[dicEditData valueForKey:TAG_ALIGNMENT] isEqualToString:TEXTALLIGNMENT_CENTER])
                textView.textAlignment = NSTextAlignmentCenter;
            else if([[dicEditData valueForKey:TAG_ALIGNMENT] isEqualToString:TEXTALLIGNMENT_LEFT])
                textView.textAlignment = NSTextAlignmentLeft;
            else
                textView.textAlignment = NSTextAlignmentRight;
            
            btnPainBrush.backgroundColor = [dicEditData valueForKey:TAG_TEXT_COLOR];
            [textView setNeedsDisplay];
        }
        else
        {
            textView.text = @"";
            containerView.hidden = TRUE;
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

#pragma mark -
#pragma mark WEPopoverControllerDelegate implementation

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)thePopoverController {
	//Safe to release the popover here
	self.wePopoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)thePopoverController {
	//The popover is automatically dismissed if you click outside it, unless you return NO here
	return YES;
}
-(void) colorPopoverControllerDidSelectColor:(NSString *)hexColor
{
    textView.textColor = [GzColors colorFromHex:hexColor];
    
    btnPainBrush.backgroundColor = [GzColors colorFromHex:hexColor];
    
    [textView setNeedsDisplay];
    [self.wePopoverController dismissPopoverAnimated:YES];
    self.wePopoverController = nil;
}


#pragma mark - Button Method
- (IBAction)btnNextClicked:(id)sender
{
    @try
    {
        
        if(carousel.hidden == FALSE)
        {
            
            UIImage *img =  [UIImage imageNamed:[NSString stringWithFormat:@"eCard%d.jpg",selectViewIndex]];
            imgViewMessage.image = img;
            imgPreview.image = img;
            
            
            holdingIndex = 2;
            imgSteps.image = imgStep2;
            lblMessage.text = MSG_TYPEMESSAGE;
            [self addObserverIntoView];
            [carousel fadeOut:0.5 delegate:self];
//            viewMessage.hidden = FALSE;
//            [viewMessage slideInFrom:kFTAnimationRight duration:0.5 delegate:nil];
            
            imgPreview.hidden = FALSE;
            viewPreview.hidden = FALSE;
            [viewPreview slideInFrom:kFTAnimationRight duration:0.5 delegate:nil];
            
            
            rectViewMessage = viewMessage.frame;
        }
        else if(viewPreview.hidden == FALSE)
        {
            holdingIndex = 3;
            [self removeObserverFromView];
            imgSteps.image = imgStep3;
            lblMessage.text = MSG_SENDEMAIL;
            UIButton *btn = (id)sender;
            [btn setTitle:@"SEND" forState:UIControlStateNormal];
            
//            [viewMessage fadeOut:0.5 delegate:self];
            [viewPreview fadeOut:0.5 delegate:self];
            
            rectViewEmailAddress = viewEmailAddress.frame;
            viewEmailAddress.hidden = TRUE;
            [viewEmailAddress slideInFrom:kFTAnimationRight duration:0.5 delegate:nil];
        }
        else
        {
            NSString *strEmail = [txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if(strEmail.length == 0)
            {
                [SVProgressHUD showErrorWithStatus:MSG_ENTEREMAIL];
            }
            else if(![StringValidation validateEmail:strEmail])
            {
                [SVProgressHUD showErrorWithStatus:MSG_INVALIDEMAIL];
            }
            else
            {
                [self calleCard];
//                [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
//                [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
//                [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(success) userInfo:nil repeats:NO];
            }
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}
-(void)success
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APPLICATIONNAME message:MSG_ECARDSUBMIT delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    alert.tag = 0;
    [alert show];
//    [SVProgressHUD showSuccessWithStatus:MSG_ECARDSUBMIT];
//    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(moveToHomeScreen) userInfo:nil repeats:NO];

}
#pragma mark - AlertView Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 0) // Buy Success Message
    {
        [self moveToHomeScreen];
    }
}
- (IBAction)btneCardButton:(id)sender
{
    @try
    {
        UIButton *btn = (id)sender;
        
        
        [textView resignFirstResponder];
        [txtEmail resignFirstResponder];
        int currentIndex = (int)btn.tag;
        
        if(currentIndex < holdingIndex)
        {
            if(currentIndex != holdingIndex)
            {
                if(currentIndex == 1)
                {
                    // Step 1
                    imgSteps.image = imgStep1;
                    lblMessage.text = MSG_SELECTTEMPLATE;
                    holdingIndex = 1;
                    carousel.hidden = FALSE;
                    imgPreview.hidden = TRUE;
                    [btnNextSend setTitle:@"NEXT" forState:UIControlStateNormal];
                    [carousel slideInFrom:kFTAnimationLeft duration:0.5 delegate:nil];
                    //[viewMessage fadeOut:ANIMATION_TIME delegate:self];
                    
                    //[viewPreview fadeOut:ANIMATION_TIME delegate:self];
                    viewPreview.hidden = TRUE;
                    [viewEmailAddress fadeOut:ANIMATION_TIME delegate:self];
                    //                    viewMessage.hidden = TRUE;
                    //                    viewEmailAddress.hidden = TRUE;
                    
                    
                    // Reset the Image View and textView for editing
                    textView.text = @"";
                    imgPreview.image = nil;
                    imgViewMessage.image = nil;
                    containerView.hidden = TRUE;
                    
                    dicEditData = nil;
                    btnLeft.selected = TRUE;
                    btnCenter.selected = FALSE;
                    btnRight.selected = FALSE;
                    // End
                    
                    
                }
                else if(currentIndex == 2)
                {
                    // Step 2
                    imgSteps.image = imgStep2;
                    lblMessage.text = MSG_TYPEMESSAGE;
                    [self addObserverIntoView];
                    holdingIndex = 2;
                    //viewMessage.hidden = FALSE;
                    viewPreview.hidden = FALSE;
                    imgPreview.hidden = FALSE;
                    [btnNextSend setTitle:@"NEXT" forState:UIControlStateNormal];
                    //[viewMessage slideInFrom:kFTAnimationLeft duration:0.5 delegate:nil];
                    [viewPreview slideInFrom:kFTAnimationLeft duration:0.5 delegate:nil];
                    [carousel fadeOut:ANIMATION_TIME delegate:self];
                    [viewEmailAddress fadeOut:ANIMATION_TIME delegate:self];
                    
                }
            }
        }
        else
        {
            return;
            int newIndex = holdingIndex + 1;
            if(holdingIndex == 1) //Message
            {
                holdingIndex = 2;
                imgSteps.image = imgStep2;
                lblMessage.text = MSG_TYPEMESSAGE;
                [self addObserverIntoView];
                [carousel fadeOut:0.5 delegate:self];
                //viewMessage.hidden = FALSE;
                viewPreview.hidden = FALSE;
                imgPreview.hidden = FALSE;
                [btnNextSend setTitle:@"NEXT" forState:UIControlStateNormal];
                
                //[viewMessage slideInFrom:kFTAnimationRight duration:0.5 delegate:nil];
                [viewPreview slideInFrom:kFTAnimationRight duration:0.5 delegate:nil];
                
                rectViewMessage = viewMessage.frame;
            }
            else if(holdingIndex == 2) // Send
            {
                holdingIndex = 3;
                [self removeObserverFromView];
                imgSteps.image = imgStep3;
                lblMessage.text = MSG_SENDEMAIL;
                [btnNextSend setTitle:@"SEND" forState:UIControlStateNormal];
                //[viewMessage fadeOut:0.5 delegate:self];
                [viewPreview fadeOut:0.5 delegate:self];
                rectViewEmailAddress = viewEmailAddress.frame;
                viewEmailAddress.hidden = TRUE;
                [viewEmailAddress slideInFrom:kFTAnimationRight duration:0.5 delegate:nil];
                
            }
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnEditClicked:(id)sender
{
    [appDelegate beginIgnoreEvent];
    isEditView = TRUE;
    viewEdit.hidden = FALSE;
    [self.view bringSubviewToFront:viewEdit];
    [self setValueOnView];
    [viewEdit slideInFrom:kFTAnimationBottom duration:ANIMATION_TIME delegate:self];
    [NSTimer scheduledTimerWithTimeInterval:ANIMATION_TIME target:appDelegate selector:@selector(endIgnoreEvent) userInfo:nil repeats:NO];
}

-(void)moveToHomeScreen
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    HomeViewController *demoController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    demoController.title = [NSString stringWithFormat:@"Home"];
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:demoController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

}
#pragma mark - Call Album List View Service
-(void)calleCard
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
 
    [self storedFileIntoLocal];
    NSMutableDictionary *dicParmeter = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *dicUser = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_DATA"];
    [dicParmeter setValue:[dicUser valueForKey:KEY_EMAIL] forKey:KEY_EMAIL];
    [dicParmeter setValue:appDelegate.strCustomerID forKey:KEY_CUSTOMERID];
    
    [dicParmeter setValue:txtEmail.text forKey:KEY_CUSTOMEREMAIL];
    
    
//    NSData *imageData =  UIImageJPEGRepresentation(imgPreview.image, 0.6);
    
    NSString *strImageName = @"imagePreview.jpeg";
    NSString *imagePath = [CACHE_DIRECTORY stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",strImageName]];

    
    [dicParmeter setValue:imagePath forKey:KEY_FILE];
    
    [WebAPIRequest callWebSevice:self webServiceName:WS_ECARD parameterDic:dicParmeter];
    dicParmeter = nil;
    
}


-(void)storedFileIntoLocal
{
    NSString *strImageName = @"imagePreview.jpeg";
    NSString *imagePath = [CACHE_DIRECTORY stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",strImageName]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL fileExists = [fileManager fileExistsAtPath:imagePath];
    
    if(fileExists)
    {
        BOOL remove = [fileManager removeItemAtPath:imagePath error:nil];
    }
    NSData *imageData = UIImageJPEGRepresentation(imgPreview.image, 0.5);
    [imageData writeToFile:imagePath atomically:YES];

}
#pragma mark - Web-Service Response Method
-(void)uploadRequestFinished:(ASIHTTPRequest *)request
{
    @try {
        [appDelegate endIgnoreEvent];
        [SVProgressHUD dismiss];
        NSString *responseString = [request responseString];
        
        NSDictionary *returnDict;
        if([responseString length])
        {
            returnDict = [responseString JSONValue];
        }
        
        if(returnDict)
        {
            if([[returnDict valueForKey:KEY_SUCCESS] intValue] == 0)
            {
                [SVProgressHUD showErrorWithStatus:[returnDict valueForKey:KEY_MSG]];
            }
            else
            {
                [self success];
            }
        }
        
        
        responseString = nil;
        returnDict = nil;
        
    }
    @catch (NSException *exception) {
        NSLog(@"%s ==> %@",__FUNCTION__,exception.description);
    }
}
- (void)uploadRequestFailed:(ASIHTTPRequest *)request
{
    [appDelegate endIgnoreEvent];
    [SVProgressHUD showErrorWithStatus:MSG_SERVERERROR];
}

@end
