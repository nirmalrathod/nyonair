//
//  eCardViewController.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "HPGrowingTextView.h"
#import <QuartzCore/QuartzCore.h>
#import "WEPopoverController.h"
#import "ColorViewController.h"
#import "ImageEditor.h"
@interface eCardViewController : UIViewController<iCarouselDataSource, iCarouselDelegate,HPGrowingTextViewDelegate,WEPopoverControllerDelegate, UIPopoverControllerDelegate, ColorViewControllerDelegate>
{
    
    IBOutlet UIView *viewAllItems;
    CGRect originalRect;
    UIView *containerView;
    HPGrowingTextView *textView;
    float firstX;
    float firstY;

    IBOutlet UIImageView *imgSteps;
    CGRect rectViewEmailAddress;
    CGRect rectViewMessage;
    IBOutlet UITextField *txtEmail;
    IBOutlet UIView *viewEmailAddress;
    IBOutlet UIView *viewMessage;
    IBOutlet iCarousel *carousel;
    int selectViewIndex;
    IBOutlet UILabel *lblMessage;
    
    IBOutlet UIButton *btnNextSend;
    IBOutlet UIImageView *imgViewMessage;
    UIImage *imgSelectedeCardImage;
    int holdingIndex;
    CGRect rectView;
    IBOutlet UIImageView *imgPreview;
    IBOutlet UIView *viewPreview;
    IBOutlet UIView *viewEdit;
    IBOutlet UIButton *btnPainBrush;
    
    NSMutableDictionary *dicEditData;
    IBOutlet UIButton *btnBold, *btnCenter,*btnLeft,*btnRight;
    
    BOOL isEditView;

}
@property (nonatomic, strong) WEPopoverController *wePopoverController;
@property (nonatomic, retain) IBOutlet iCarousel *carousel;

- (IBAction)btneCardButton:(id)sender;
- (IBAction)btnEditClicked:(id)sender;


- (IBAction)btnNextClicked:(id)sender;
-(UIImage *)takeScreenShote;

-(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point;
- (IBAction)btnBoldNormalClicked:(id)sender;
- (IBAction)btnPaintBrushClicked:(id)sender;
- (IBAction)btnCenterClicked:(id)sender;
- (IBAction)btnLeftAlignmentClicked:(id)sender;
- (IBAction)btnRightAlignmentClicked:(id)sender;
- (IBAction)btnDoneClicked:(id)sender;
- (IBAction)btnCancelClicked:(id)sender;

@end
