//
//  MenuViewController.h
//  ClubGuru
//
//  Created by Nirmalsinh Rathod on 11/5/13.
//  Copyright (c) 2013 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBPathImageView.h"
#import "HomeViewController.h"
#import "eCardViewController.h"
#import "NewsViewController.h"
#import "SharingViewController.h"
#import "LoginViewController.h"
#import "ProfileViewController.h"
#import "SubscriptionViewController.h"

@interface MenuViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    NSMutableDictionary *dicData;
    NSMutableArray *aryData;
    
    IBOutlet UIButton *btnPhoto, *btneCard, *btnNews, *btnSharing, *btnLogout, *btnRegistration, *btnSubscription;
    
    
}
-(void)testmenu :(BOOL)is;
-(IBAction)btnHomeClicked:(id)sender;
-(IBAction)btneCardClicked:(id)sender;
-(IBAction)btnNewsClicked:(id)sender;
-(IBAction)btnSharingClicked:(id)sender;
-(IBAction)btnLogoutClicked:(id)sender;
-(IBAction)btnRegistrationClicked:(id)sender;
-(IBAction)btnSubscriptionClicked:(id)sender;

@end
