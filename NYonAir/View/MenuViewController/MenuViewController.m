//
//  MenuViewController.m
//  ClubGuru
//
//  Created by Nirmalsinh Rathod on 11/5/13.
//  Copyright (c) 2013 Nirmalsinh Rathod. All rights reserved.
//

#import "MenuViewController.h"
#import "MFSideMenu.h"
//#import "NearbyViewController.h"
//#import "KTVViewController.h"
//#import "SignUpViewController.h"
//#import "EventViewController.h"
//#import "HomeViewController.h"
//#import "PromotionsViewController.h"
//#import "NotifacationViewController.h"
//#import "FavoriteViewController.h"
//#import  "ProfileViewController.h"
//#import "SettingsViewController.h"
//#import "LoginViewController.h"
@interface MenuViewController ()

@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.title = @"NYonAir";

    self.view.backgroundColor = [UIColor clearColor];


    btnPhoto.selected = TRUE;
    btneCard.selected = FALSE;
    btnNews.selected = FALSE;
    btnSharing.selected = FALSE;
    btnLogout.selected = FALSE;
    btnSubscription.selected = FALSE;
    btnRegistration.selected = FALSE;

    [btnPhoto setTitle:TITLE_PHOTO forState:UIControlStateNormal];
    [btneCard setTitle:TITLE_ECARD forState:UIControlStateNormal];
    [btnNews setTitle:TITLE_NEWS forState:UIControlStateNormal];
    [btnSharing setTitle:TITLE_CONTACT forState:UIControlStateNormal];
    [btnLogout setTitle:TITLE_LOGOUT forState:UIControlStateNormal];

    [btnPhoto setTitle:TITLE_PHOTO forState:UIControlStateSelected];
    [btneCard setTitle:TITLE_ECARD forState:UIControlStateSelected];
    [btnNews setTitle:TITLE_NEWS forState:UIControlStateSelected];
    [btnSharing setTitle:TITLE_CONTACT forState:UIControlStateSelected];
    [btnLogout setTitle:TITLE_LOGOUT forState:UIControlStateSelected];

    [btnPhoto setTitle:TITLE_PHOTO forState:UIControlStateHighlighted];
    [btneCard setTitle:TITLE_ECARD forState:UIControlStateHighlighted];
    [btnNews setTitle:TITLE_NEWS forState:UIControlStateHighlighted];
    [btnSharing setTitle:TITLE_CONTACT forState:UIControlStateHighlighted];
    [btnLogout setTitle:TITLE_LOGOUT forState:UIControlStateHighlighted];

    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;


    aryData = [[NSMutableArray alloc] init];
    
    
//    if(appDelegate.isiPhone5)
//        tblView.frame = CGRectMake(0, 66, 320, 502);
//    else
//        tblView.frame = CGRectMake(0, 66, 320, 400);
    

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
/*
-(void)testmenu :(BOOL)is
{
    aryData = nil;
    aryData = [[NSMutableArray alloc] init];
    
    
    dicData = nil;
    dicData = [[NSMutableDictionary alloc] init];
    [dicData setValue:TITLE_HOME forKey:@"name"];
    [dicData setValue:@"home_icon.png" forKey:@"image"];
    [aryData addObject:dicData];
    
    if(![appDelegate.strUserID isEqualToString:@"-1"])
    {
        dicData = nil;
        dicData = [[NSMutableDictionary alloc] init];
        [dicData setValue:TITLE_NOTIFICATION forKey:@"name"];
        [dicData setValue:@"notification_icon.png" forKey:@"image"];
        [aryData addObject:dicData];
    }
    
    dicData = nil;
    dicData = [[NSMutableDictionary alloc] init];
    [dicData setValue:TITLE_EVENTS forKey:@"name"];
    [dicData setValue:@"events_icon.png" forKey:@"image"];
    [aryData addObject:dicData];
    
    dicData = nil;
    dicData = [[NSMutableDictionary alloc] init];
    [dicData setValue:TITLE_PROMOTIONS forKey:@"name"];
    [dicData setValue:@"promotions_icon.png" forKey:@"image"];
    [aryData addObject:dicData];
    
    if(![appDelegate.strUserID isEqualToString:@"-1"])
    {
        dicData = nil;
        dicData = [[NSMutableDictionary alloc] init];
        [dicData setValue:TITLE_FAVORITES forKey:@"name"];
        [dicData setValue:@"favorites_icon.png" forKey:@"image"];
        [aryData addObject:dicData];
        
        dicData = nil;
        dicData = [[NSMutableDictionary alloc] init];
        [dicData setValue:TITLE_PROFILE forKey:@"name"];
        [dicData setValue:@"blank_profile_pic.png" forKey:@"image"];
        [aryData addObject:dicData];
        
        dicData = nil;
        dicData = [[NSMutableDictionary alloc] init];
        [dicData setValue:TITLE_SETTINGS forKey:@"name"];
        [dicData setValue:@"settings_icon.png" forKey:@"image"];
        [aryData addObject:dicData];
        
        dicData = nil;
        dicData = [[NSMutableDictionary alloc] init];
        [dicData setValue:TITLE_LOGOUT forKey:@"name"];
        [dicData setValue:@"logout_icon.png" forKey:@"image"];
        [aryData addObject:dicData];
    }
    else
    {
        dicData = nil;
        dicData = [[NSMutableDictionary alloc] init];
        [dicData setValue:TITLE_LOGIN forKey:@"name"];
        [dicData setValue:@"logout_icon.png" forKey:@"image"];
        [aryData addObject:dicData];
        
    }
    
    [tblView reloadData];

    
}*/
-(void)viewWillAppear:(BOOL)animated
{
   // [self testmenu:YES];
    [self changeButtonState];
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button method
-(IBAction)btnHomeClicked:(id)sender
{
    btnPhoto.selected = TRUE;
    btneCard.selected = FALSE;
    btnNews.selected = FALSE;
    btnSharing.selected = FALSE;
    btnLogout.selected = FALSE;
    btnSubscription.selected = FALSE;
    btnRegistration.selected = FALSE;

    
    HomeViewController *demoController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    demoController.title = [NSString stringWithFormat:TITLE_PHOTO];
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:demoController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

}
-(IBAction)btneCardClicked:(id)sender
{
    
    btnPhoto.selected = FALSE;
    btneCard.selected = TRUE;
    btnNews.selected = FALSE;
    btnSharing.selected = FALSE;
    btnLogout.selected = FALSE;
    btnSubscription.selected = FALSE;
    btnRegistration.selected = FALSE;

    eCardViewController *objeCardViewController ;
    
    if(appDelegate.isiPhone5)
    {
        objeCardViewController = [[eCardViewController alloc] initWithNibName:@"eCardViewController_iPhone4" bundle:nil];
    }
    else
    {
        objeCardViewController = [[eCardViewController alloc] initWithNibName:@"eCardViewController" bundle:nil];

    }
    objeCardViewController.title = [NSString stringWithFormat:TITLE_ECARD];
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:objeCardViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

}
-(IBAction)btnNewsClicked:(id)sender
{
    
    btnPhoto.selected = FALSE;
    btneCard.selected = FALSE;
    btnNews.selected = TRUE;
    btnSharing.selected = FALSE;
    btnLogout.selected = FALSE;
    btnSubscription.selected = FALSE;
    btnRegistration.selected = FALSE;

    
    NewsViewController *objNewsViewController = [[NewsViewController alloc] initWithNibName:@"NewsViewController" bundle:nil];
    objNewsViewController.title = TITLE_NEWS;
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:objNewsViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

    
}
-(IBAction)btnSharingClicked:(id)sender
{
    
    btnPhoto.selected = FALSE;
    btneCard.selected = FALSE;
    btnNews.selected = FALSE;
    btnSharing.selected = TRUE;
    btnLogout.selected = FALSE;
    btnSubscription.selected = FALSE;
    btnRegistration.selected = FALSE;


    
    SharingViewController *objSharingViewController = [[SharingViewController alloc] initWithNibName:@"SharingViewController" bundle:nil];
    objSharingViewController.title = TITLE_CONTACT;
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:objSharingViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

}
-(IBAction)btnLogoutClicked:(id)sender
{
    btnPhoto.selected = FALSE;
    btneCard.selected = FALSE;
    btnNews.selected = FALSE;
    btnSharing.selected = FALSE;
    btnLogout.selected = TRUE;
    btnSubscription.selected = FALSE;
    btnRegistration.selected = FALSE;

    appDelegate.lastController = viewLogout;
    [[[UIAlertView alloc] initWithTitle:APPLICATIONNAME message:MSG_LOGOUT delegate:self cancelButtonTitle:BUTTON_OK otherButtonTitles:BUTTON_CANCEL, nil] show];
}
-(IBAction)btnRegistrationClicked:(id)sender
{
    btnPhoto.selected = FALSE;
    btneCard.selected = FALSE;
    btnNews.selected = FALSE;
    btnSharing.selected = FALSE;
    btnLogout.selected = FALSE;
    btnSubscription.selected = FALSE;
    btnRegistration.selected = TRUE;

    ProfileViewController *objProfileViewController = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
 
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:objProfileViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

}
-(IBAction)btnSubscriptionClicked:(id)sender
{
    btnPhoto.selected = FALSE;
    btneCard.selected = FALSE;
    btnNews.selected = FALSE;
    btnSharing.selected = FALSE;
    btnLogout.selected = FALSE;
    btnSubscription.selected = TRUE;
    btnRegistration.selected = FALSE;

    SubscriptionViewController *objProfileViewController;
    
    if(appDelegate.isiPhone5)
        objProfileViewController = [[SubscriptionViewController alloc] initWithNibName:@"SubscriptionViewController_iPhone5" bundle:nil];
    else
        objProfileViewController = [[SubscriptionViewController alloc] initWithNibName:@"SubscriptionViewController" bundle:nil];
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:objProfileViewController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    
}
-(void)changeButtonState
{
    if(appDelegate.lastController == viewHomeScreen)
    {
        btnPhoto.selected = TRUE;
        btneCard.selected = FALSE;
        btnNews.selected = FALSE;
        btnSharing.selected = FALSE;
        btnLogout.selected = FALSE;
        btnSubscription.selected = FALSE;
        btnRegistration.selected = FALSE;

    }
    else if(appDelegate.lastController == viewECard)
    {
        btnPhoto.selected = FALSE;
        btneCard.selected = TRUE;
        btnNews.selected = FALSE;
        btnSharing.selected = FALSE;
        btnLogout.selected = FALSE;
        btnSubscription.selected = FALSE;
        btnRegistration.selected = FALSE;

    }
    else if(appDelegate.lastController == viewNews)
    {
        
        btnPhoto.selected = FALSE;
        btneCard.selected = FALSE;
        btnNews.selected = TRUE;
        btnSharing.selected = FALSE;
        btnLogout.selected = FALSE;
        btnSubscription.selected = FALSE;
        btnRegistration.selected = FALSE;
        

    }
    else if(appDelegate.lastController == viewContact)
    {
        btnPhoto.selected = FALSE;
        btneCard.selected = FALSE;
        btnNews.selected = FALSE;
        btnSharing.selected = TRUE;
        btnLogout.selected = FALSE;
        btnSubscription.selected = FALSE;
        btnRegistration.selected = FALSE;

    }
    else if(appDelegate.lastController == viewSubscription)
    {
        btnPhoto.selected = FALSE;
        btneCard.selected = FALSE;
        btnNews.selected = FALSE;
        btnSharing.selected = FALSE;
        btnLogout.selected = FALSE;
        btnSubscription.selected = TRUE;
        btnRegistration.selected = FALSE;

    }
    else if(appDelegate.lastController == viewProfile)
    {
        btnPhoto.selected = FALSE;
        btneCard.selected = FALSE;
        btnNews.selected = FALSE;
        btnSharing.selected = FALSE;
        btnLogout.selected = FALSE;
        btnSubscription.selected = FALSE;
        btnRegistration.selected = TRUE;

    }
    else if(appDelegate.lastController == viewLogout)
    {
        btnPhoto.selected = FALSE;
        btneCard.selected = FALSE;
        btnNews.selected = FALSE;
        btnSharing.selected = FALSE;
        btnLogout.selected = TRUE;
        btnSubscription.selected = FALSE;
        btnRegistration.selected = FALSE;

    }


}
#pragma mark - UITableViewDataSource

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        static NSString *MyIdentifier = @"MyIdentifier";
        MyIdentifier = @"tblCellView";
        
        __weak MenuCellViewController *cell = (MenuCellViewController *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if(cell == nil) {
            [[NSBundle mainBundle] loadNibNamed:@"MenuCellViewController" owner:self options:nil];
            cell = objMenu;
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.lblNumber.layer.cornerRadius = 5.0;
            
//            CGContextRef context = UIGraphicsGetCurrentContext();
//            CGContextSaveGState(context);
//            addRoundedRectToPath(context, cell.imgThumb.frame, 10, 10);
//            CGContextClip(context);
//            [cell.imgThumb.image drawInRect:cell.imgThumb.frame];
//            CGContextRestoreGState(context);
        }
        
        NSString *strTitle = [[aryData objectAtIndex:indexPath.row] valueForKey:@"name"];
        if(![strTitle isEqualToString:TITLE_PROFILE])
        {

        cell.imgThumb.image = [UIImage imageNamed:[[aryData objectAtIndex:indexPath.row] valueForKey:@"image"]];
        cell.lblTitle.text = [[aryData objectAtIndex:indexPath.row] valueForKey:@"name"];
            cell.imgThumb.hidden = FALSE;
        }
        else
        {
//            cell.imgThumb.image = [UIImage imageNamed:@"blank_profile_picTEST.png"];
            CALayer * l = [cell.imgThumb layer];
            [l setMasksToBounds:YES];
            [l setCornerRadius:20.0];
            
            // You can even add a border
            [l setBorderWidth:2.0];
            [l setBorderColor:[[UIColor redColor] CGColor]];

            cell.imgThumb.hidden = FALSE;
            
            NSString *strImageURL = [NSString stringWithFormat:@"%@",[appDelegate.dicUser valueForKey:@"thumb_media_path"]];
//            strImageURL =@"http://media.santabanta.com/newsite/cinemascope/feed/akki19.jpg";
            

            [cell.imgThumb setImageWithURL:[NSURL URLWithString:strImageURL]
                             placeholderImage:noImage
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                                    }];
            

            
            cell.lblTitle.text = [NSString stringWithFormat:@"%@ %@",[appDelegate.dicUser valueForKey:@"firstname"],[appDelegate.dicUser valueForKey:@"lastname"]];
        }
        
        if([strTitle isEqualToString:TITLE_NOTIFICATION])
            cell.lblNumber.hidden = FALSE;
        else
            cell.lblNumber.hidden = TRUE;
        return cell;
        
    }
    @catch (NSException *e) {
        
    }
    return nil;
}
*/

#pragma mark - UITableViewDelegate
/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *str =[[aryData objectAtIndex:indexPath.row] valueForKey:@"name"];
    if([str isEqualToString:TITLE_HOME]) // Home
    {
        HomeViewController *demoController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"Home"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

    }
    else if([str isEqualToString:TITLE_NOTIFICATION]) // Notification
    {
        NotifacationViewController *demoController = [[NotifacationViewController alloc] initWithNibName:@"NotifacationViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"Nofi"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    else if([str isEqualToString:TITLE_EVENTS]) // Events
    {
        EventViewController *demoController = [[EventViewController alloc] initWithNibName:@"EventViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"KTV"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    else if([str isEqualToString:TITLE_PROMOTIONS]) // Promotions
    {
        PromotionsViewController *demoController = [[PromotionsViewController alloc] initWithNibName:@"PromotionsViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"Promotion"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    else if([str isEqualToString:TITLE_FAVORITES]) // Favorites
    {
        FavoriteViewController *demoController = [[FavoriteViewController alloc] initWithNibName:@"FavoriteViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"Fav"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    else if([str isEqualToString:TITLE_PROFILE]) // Profile
    {
        ProfileViewController *demoController = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"Pro"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    else if([str isEqualToString:TITLE_SETTINGS]) // Setings
    {
        SettingsViewController *demoController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"Settings"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    else if([str isEqualToString:TITLE_LOGIN]) // Login
    {
        appDelegate.current_service = NONE;
        
        LoginViewController *demoController = [[LoginViewController  alloc] initWithNibName:@"LoginViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"Login"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    else if([str isEqualToString:TITLE_LOGOUT]) // Logout
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APPLICATIONNAME message:MESSAGE_LOGOUT delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
        alert.tag = 1;
        [alert show];
    }
    
    
    
    
    
}
*/
#pragma mark - Alert View Delegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        
        LoginViewController *objHome;
        
        if(appDelegate.isiPhone5)
            objHome = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        else
            objHome = [[LoginViewController alloc] initWithNibName:@"LoginViewController_iPhone4" bundle:nil];

        UINavigationController *objLoginNavigation = [[UINavigationController alloc] initWithRootViewController:objHome];
            
            appDelegate.window.rootViewController = objLoginNavigation;
            [appDelegate.window makeKeyAndVisible];
        
    }
}

#pragma mark - Web Service Responce
/*
-(void)uploadRequestFinished:(ASIHTTPRequest *)request
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    NSString *responseString = [request responseString];
    
    NSDictionary *returnDict;
    if([responseString length])
    {
        returnDict = [responseString JSONValue];
    }
    NSLog(@"ResponseString:%@",returnDict.description);
    
    if(returnDict)
    {
        if([[returnDict valueForKey:@"status"] intValue] == 0)
            [SVProgressHUD showErrorWithStatus:[returnDict valueForKey:@"message"]];
        else if([[returnDict valueForKey:@"status"] intValue] == 1)
        {
//            [SVProgressHUD showSuccessWithStatus:[returnDict valueForKey:@"message"]];
            [SVProgressHUD dismiss];
            HomeViewController *demoController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
            demoController.title = [NSString stringWithFormat:@"Home"];
            
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:demoController];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

            
        }
    }
    else
    {
        [SVProgressHUD showErrorWithStatus:MESSAGE_SERVER_ISSUE];
    }
}
- (void)uploadRequestFailed:(ASIHTTPRequest *)request
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD showErrorWithStatus:MESSAGE_SERVER_ISSUE];
    NSError *error = [request error];
    NSLog(@"Error:%@",[error description]);
}
*/

@end
