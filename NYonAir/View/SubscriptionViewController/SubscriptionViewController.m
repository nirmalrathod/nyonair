//
//  SubscriptionViewController.m
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 5/7/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import "SubscriptionViewController.h"

@interface SubscriptionViewController ()

@end

@implementation SubscriptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    // Set Method for Bar Button and left menu
    [self setupMenuBarButtonItems];
    // End
    
    self.title = TITLE_SUBSCRIPTION;

    viewNormal.layer.borderColor = [UIColor blackColor].CGColor;
    viewNormal.layer.borderWidth = 1;
    viewNormal.layer.cornerRadius = 2;

    viewPrimium.layer.borderColor = [UIColor blackColor].CGColor;
    viewPrimium.layer.borderWidth = 1;
    viewPrimium.layer.cornerRadius = 2;
    
//    btnNormal.selected = FALSE;
//    btnPremium.selected = TRUE;

    viewNormalSelected.hidden = FALSE;
    btnNormal.hidden = TRUE;
    
    viewPremiumSelected.hidden = TRUE;
    btnPremium.hidden = FALSE;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    appDelegate.lastController = viewSubscription;
    [super viewWillAppear:animated];
}
-(IBAction)btnNormalClicked:(id)sender
{
//    btnNormal.selected = TRUE;
//    btnPremium.selected = FALSE;
    
    viewNormalSelected.hidden = FALSE;
    btnNormal.hidden = TRUE;
    
    viewPremiumSelected.hidden = TRUE;
    btnPremium.hidden = FALSE;

}
-(IBAction)btnPremiumClicked:(id)sender
{
//    btnNormal.selected = FALSE;
//    btnPremium.selected = TRUE;

    [self calleCardImage];
//    viewNormalSelected.hidden = TRUE;
//    btnNormal.hidden = FALSE;
//    
//    viewPremiumSelected.hidden = FALSE;
//    btnPremium.hidden = TRUE;

}

#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems {
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
        self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    } else {
        self.navigationItem.leftBarButtonItem = nil;//[self leftMenuBarButtonItem];
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:IMAGE_MENU style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(leftSideMenuButtonPressed:)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 22.0f, 15.0f)];
    [a1 addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_MENU forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

- (UIBarButtonItem *)backBarButtonItem {
    
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 20.0, 20.0f)];
    [a1 setImageEdgeInsets:UIEdgeInsetsMake(0, -9, 0, 0)];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
        a1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [a1 addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_BACK forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}

- (void)rightSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Call Album List View Service
-(void)calleCardImage
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
    
    NSMutableDictionary *dicParmeter = [[NSMutableDictionary alloc] init];
    [dicParmeter setValue:appDelegate.strCustomerID forKey:KEY_CUSTOMERID];
    [WebAPIRequest callWebSevice:self webServiceName:WS_SUBSCRIPTION parameterDic:dicParmeter];
    dicParmeter = nil;
    
}
#pragma mark - Web-Service Response Method
-(void)uploadRequestFinished:(ASIHTTPRequest *)request
{
    @try {
        [appDelegate endIgnoreEvent];
        [SVProgressHUD dismiss];
        NSString *responseString = [request responseString];
        
        NSDictionary *returnDict;
        if([responseString length])
        {
            returnDict = [responseString JSONValue];
        }
        
        if(returnDict)
        {
            if([[returnDict valueForKey:KEY_SUCCESS] intValue] == 0)
            {
                [SVProgressHUD showErrorWithStatus:[returnDict valueForKey:KEY_MSG]];
            }
            else
            {
                [SVProgressHUD showSuccessWithStatus:[returnDict valueForKey:KEY_MSG]];
                viewNormalSelected.hidden = TRUE;
                btnNormal.hidden = FALSE;
                
                viewPremiumSelected.hidden = FALSE;
                btnPremium.hidden = TRUE;

            }
        }
        
        
        responseString = nil;
        returnDict = nil;
        
    }
    @catch (NSException *exception) {
        NSLog(@"%s ==> %@",__FUNCTION__,exception.description);
    }
}
- (void)uploadRequestFailed:(ASIHTTPRequest *)request
{
    [appDelegate endIgnoreEvent];
    [SVProgressHUD showErrorWithStatus:MSG_SERVERERROR];
}

@end
