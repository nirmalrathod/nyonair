//
//  SubscriptionViewController.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 5/7/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubscriptionViewController : UIViewController
{
    IBOutlet UIView *viewNormal;
    
    IBOutlet UIView *viewPrimium;
    
    IBOutlet UIButton *btnNormal, *btnPremium;
    
    IBOutlet UIView *viewNormalSelected, *viewPremiumSelected;
    
}
-(IBAction)btnNormalClicked:(id)sender;
-(IBAction)btnPremiumClicked:(id)sender;
@end
