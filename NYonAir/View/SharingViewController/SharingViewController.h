//
//  SharingViewController.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SharingViewController : UIViewController
{
    
}
@property (nonatomic, retain) UIDocumentInteractionController *dic;
-(void)followOnInstagram;
- (IBAction)btnVisitUsClicked:(id)sender;
- (IBAction)btnInstagramClicked:(id)sender;
- (IBAction)btnLinkedInClicked:(id)sender;
- (IBAction)btnTwitterClicked:(id)sender;
- (IBAction)btnFacebookClicked:(id)sender;
@end
