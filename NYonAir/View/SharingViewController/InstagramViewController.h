//
//  InstagramViewController.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 5/19/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SharingViewController;
@interface InstagramViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *webView;
}
@property (nonatomic, weak) SharingViewController *objSharingViewController;

@end
