//
//  SharingViewController.m
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import "SharingViewController.h"
#import "FacebookLikeViewController.h"
#import "FHSTwitterEngine.h"
#import "InstagramKit.h"
#import "InstagramMedia.h"
#import "InstagramUser.h"
#import "InstagramViewController.h"
@interface SharingViewController ()<FHSTwitterEngineAccessTokenDelegate>

@end

@implementation SharingViewController
@synthesize dic;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    // Set Method for Bar Button and left menu
    [self setupMenuBarButtonItems];
    // End
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(followOnInstagram) name:NOTIFICAITON_INSTAGRAMFOLLOW object:nil];


    self.title = TITLE_CONTACT;
    
    [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:@"BOUdLEpe1qDY9AuLsEsZPpKjo" andSecret:@"5fvo7I8o8nH5dwzxfgaILhs7Qk8IhV2YGaXPvUvALU"];
    [[FHSTwitterEngine sharedEngine]setDelegate:self];
    [[FHSTwitterEngine sharedEngine]loadAccessToken];

    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    appDelegate.lastController = viewContact;
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems {
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
        self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    } else {
        self.navigationItem.leftBarButtonItem = nil;//[self leftMenuBarButtonItem];
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:IMAGE_MENU style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(leftSideMenuButtonPressed:)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 22.0f, 15.0f)];
    [a1 addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_MENU forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

- (UIBarButtonItem *)backBarButtonItem {
    
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 20.0, 20.0f)];
    [a1 setImageEdgeInsets:UIEdgeInsetsMake(0, -9, 0, 0)];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
        a1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [a1 addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_BACK forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}

- (void)rightSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}
#pragma mark - Other Memory Method
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Button Method
- (IBAction)btnVisitUsClicked:(id)sender
{
    FacebookLikeViewController *objFacebookLikeViewController = [[FacebookLikeViewController alloc] initWithNibName:@"FacebookLikeViewController" bundle:nil];
    objFacebookLikeViewController.strLikePageURL =WEBPAGE_GALLERY;
    objFacebookLikeViewController.strTitle = TITLE_VISITUS;
    [self.navigationController pushViewController:objFacebookLikeViewController animated:YES];

}
- (IBAction)btnInstagramClicked:(id)sender
{
    @try {
        
        InstagramViewController *objInstagramViewController = [[InstagramViewController alloc] initWithNibName:@"InstagramViewController" bundle:nil];
        [self.navigationController pushViewController:objInstagramViewController animated:YES];
        return;
        InstagramEngine *sharedEngine = [InstagramEngine sharedEngine];
        
        if (sharedEngine.accessToken)
        {
            InstagramViewController *objInstagramViewController = [[InstagramViewController alloc] initWithNibName:@"InstagramViewController" bundle:nil];
            //[self presentViewController:objInstagramViewController animated:YES completion:nil];
            [self.navigationController pushViewController:objInstagramViewController animated:YES];
        }
        else
        {
            [self followOnInstagram];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%s --> %@",__FUNCTION__,exception.description);
    }
}

-(void)followOnInstagram
{
    
    
//    CGRect rect = CGRectMake(0 ,0 , 0, 0);
//    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
//    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIGraphicsEndImageContext();
//    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/test.igo"];
//    NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];
//    self.dic.UTI = @"com.instagram.photo";
//    self.dic = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
//    self.dic=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
//    [self.dic presentOpenInMenuFromRect: rect    inView: self.view animated: YES ];
//    return;
    
    [appDelegate beginIgnoreEvent];
    [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];

    [[InstagramEngine sharedEngine] followuser:[NSString stringWithFormat:@"users/%@/relationship",INSTAGRAM_USERID] withSuccess:^(NSArray *users, InstagramPaginationInfo *paginationInfo) {
        NSLog(@"%@",users.description);
    }failure:^(NSError *error) {
        NSLog(@"user search failed");
    }];

}
- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    return interactionController;
}

- (IBAction)btnLinkedInClicked:(id)sender
{
}

- (IBAction)btnTwitterClicked:(id)sender
{
    UIViewController *loginController = [[FHSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success) {
        [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(likePageOnTwitter) userInfo:nil repeats:NO];

    }];
    [self presentViewController:loginController animated:YES completion:nil];


}
-(void)likePageOnTwitter
{
    NSError *error = [[FHSTwitterEngine sharedEngine] followUser:@"NYonAir" isID:FALSE];
    [SVProgressHUD dismiss];
    if(!error)
    {
        [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Yuppppppii" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else
    {
        NSLog(@"%@",error);
    }
}

- (IBAction)btnFacebookClicked:(id)sender
{
    FacebookLikeViewController *objFacebookLikeViewController = [[FacebookLikeViewController alloc] initWithNibName:@"FacebookLikeViewController" bundle:nil];
    objFacebookLikeViewController.strLikePageURL = WEBPAGE_FACEBOOK;
    objFacebookLikeViewController.strTitle = TITLE_FACEBOOKLIKE;
    [self.navigationController pushViewController:objFacebookLikeViewController animated:YES];
}
@end
