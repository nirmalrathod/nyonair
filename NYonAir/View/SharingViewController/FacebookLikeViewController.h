//
//  FacebookLikeViewController.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 5/7/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FacebookLikeViewController : UIViewController <UIWebViewDelegate>
{
    
    IBOutlet UIWebView *webView;
    NSString *strLikePageURL, *strTitle;
}
@property (strong, nonatomic)NSString *strLikePageURL, *strTitle;
@end
