//
//  DetailViewController.m
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import "DetailViewController.h"
#import "SubscriptionViewController.h"
#define normalImage [UIImage imageNamed:@"roundOrange.png"]
#define selectedImage [UIImage imageNamed:@"greenOrange.png"]
@interface DetailViewController ()

@end

@implementation DetailViewController
@synthesize carousel,strTitle;
@synthesize coinView,strAlbumId;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    // Add Title Label into Navigation bar
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.clipsToBounds = YES;
    label.font = [UIFont fontWithName:FONTNAME size:17.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.numberOfLines = 2;
    label.text = strTitle;
    self.navigationItem.titleView = label;
    [label sizeToFit];
    // End
    
    // Flip View
    [coinView setPrimaryView: viewPrice];
    [coinView setSecondaryView:viewBuyNow];
    [coinView setSpinTime:0.5];
    // End
    
    // Set Notification when Buy Button Clicked
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(buyNotificaion) name:NOTIFICAITON_BUYCLICKED object:nil];
    // End

    // Set Method for Bar Button and left menu
    [self setupMenuBarButtonItems];
    // End
    
//    btnBuy.adjustsImageWhenHighlighted = YES;
    btnBuy.highlighted = TRUE;
    hLayout.hAlignment = UIControlContentHorizontalAlignmentLeft;
    hLayout.vAlignment = UIControlContentVerticalAlignmentTop;
//    hLayout.topMargin = 0;
    hLayout.spacing = 0;
    hLayout.leftMargin = 10;
    hLayout.rightMargin = 10;

    hLayout.pagingEnabled = TRUE;
//    [self addImageIntoLayout];

    
    aryImage = nil;
    carousel.type = iCarouselTypeLinear;
    carousel.scrollEnabled = FALSE;
    carousel.bounces = FALSE;
    [carousel reloadData];
    
    UISwipeGestureRecognizer *swipeGestureLeftObjectViewVRM = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft)] ;
    swipeGestureLeftObjectViewVRM.numberOfTouchesRequired = 1;
    swipeGestureLeftObjectViewVRM.direction = (UISwipeGestureRecognizerDirectionLeft);
    [carousel addGestureRecognizer:swipeGestureLeftObjectViewVRM];
    
    UISwipeGestureRecognizer *swipeGestureRightObjectViewVRM = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)] ;
    swipeGestureRightObjectViewVRM.numberOfTouchesRequired = 1;
    swipeGestureRightObjectViewVRM.direction = (UISwipeGestureRecognizerDirectionRight);
    [carousel addGestureRecognizer:swipeGestureRightObjectViewVRM];

    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView)];
    gesture.numberOfTapsRequired = 1;
    [carousel addGestureRecognizer:gesture];
    [self.view addGestureRecognizer:gesture];
    
    
    [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(callDetailService) userInfo:nil repeats:NO];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.title = @"";
    self.navigationItem.leftBarButtonItem = nil;
    [super viewWillDisappear:animated];
}
-(void)swipeRight
{
    if(carousel.currentItemIndex > 0)
    {
        [carousel scrollToItemAtIndex:carousel.currentItemIndex-1 animated:YES];
    }
}
-(void)swipeLeft
{
    if(carousel.currentItemIndex != 10)
    {
        [carousel scrollToItemAtIndex:carousel.currentItemIndex+1 animated:YES];
        
    }
}
#pragma mark - Buy Notification Clicked
-(void)buyNotificaion
{
    @try
    {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(successBuyCalled) userInfo:nil repeats:NO];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s --> %@",__FUNCTION__,exception.description);
    }
}
-(void)successBuyCalled
{
    @try
    {
        [SVProgressHUD dismiss];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:MSG_THANKYOUTITLE message:MSG_THANKYOUMESSAGE delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag = 0;
        [alert show];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s --> %@",__FUNCTION__,exception.description);
    }
}
#pragma mark - AlertView Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 0) // Buy Success Message
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(alertView.tag == 1) // BuyerSubscription
    {
        if(buttonIndex == 0)
        {
            SubscriptionViewController *demoController = [[SubscriptionViewController alloc] initWithNibName:@"SubscriptionViewController" bundle:nil];
            
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:demoController];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

        }
    }
}
#pragma mark - Tap Gesture Method
-(void)tapView
{
    int flage = [[[NSUserDefaults standardUserDefaults] valueForKey:NOTIFICAITON_BUYFLIP] intValue];
    if(flage == 0)
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICAITON_BUYFLIP object:nil];
}

#pragma mark - Add Image in HLayoutView
-(void)addImageIntoLayout
{
    @try
    {
        for (int i = 1; i < 10 ;i++)
        {
            UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"img%d.png",i]];
            UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
            imgView.frame = CGRectMake(0, 0, hLayout.frame.size.width, 300);
            imgView.backgroundColor = [UIColor clearColor];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            
            imgView.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth);
            
            
            [hLayout addSubview:imgView];
            
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}



#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return aryImage.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    @try
    {
        
        NSString *strImageName;
        NSArray *ary = [[[aryImage objectAtIndex:index] valueForKey:KEY_IMAGEURL] componentsSeparatedByString:@"/"];
        strImageName = [ary lastObject];
        strImageName = [strImageName stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *imagePath = [CACHE_DIRECTORY stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",strImageName]];
        UIImage *img = [UIImage imageWithContentsOfFile:imagePath];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:img];

        
        
            CGRect viewFrame = CGRectMake(0, 0, 200, 320);
//            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"dtl%d.jpg",index]]];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView.frame = viewFrame;
        
            imgView.center = CGPointMake(viewFrame.size.width/2, viewFrame.size.height/2);
            view = [[UIView alloc] initWithFrame:viewFrame];
            [view setBackgroundColor:[UIColor clearColor]];
            [view addSubview:imgView];
        return view;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}


- (NSUInteger)numberOfPlaceholdersInCarousel:(iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 2;
}

- (UIView *)carousel:(iCarousel *)carousel placeholderViewAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    return nil;
}

- (CATransform3D)carousel:(iCarousel *)_carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * carousel.itemWidth);
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
}

#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems {
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
        self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    } else {
        self.navigationItem.leftBarButtonItem = nil;//[self leftMenuBarButtonItem];
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:IMAGE_MENU style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(leftSideMenuButtonPressed:)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 22.0f, 15.0f)];
    [a1 addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_MENU forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

- (UIBarButtonItem *)backBarButtonItem {
    
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 20.0, 20.0f)];
    [a1 setImageEdgeInsets:UIEdgeInsetsMake(0, -9, 0, 0)];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
        a1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [a1 addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_BACK forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}

- (void)rightSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}
#pragma mark - Other Memory Method
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Method
- (IBAction)btnViewClicked:(id)sender
{
    @try
    {
        int count = [lblView.text intValue];
        if(btnView.selected)
        {
            count --;
        }
        else
        {
            count ++;
        }
        lblView.text = [appDelegate convertIntoKserias:count];
        btnView.selected = !btnView.selected;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}
- (IBAction)btnLikeClicked:(id)sender
{
    @try
    {
        [self callFavoriteService];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnBuyNowClicked:(id)sender
{
    @try
    {
        UIButton *btn = (id)sender;
        
        if(btn.selected)
        {
            [btn setImage:selectedImage forState:UIControlStateHighlighted];
            CGContextRef context = UIGraphicsGetCurrentContext();
            context = UIGraphicsGetCurrentContext();
            [UIView beginAnimations:nil context:context];
            [UIView setAnimationDuration:0.75];
            btn.selected = FALSE;
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:btn cache:YES];
            [UIView commitAnimations];
        }
        else
        {
            [btn setImage:normalImage forState:UIControlStateHighlighted];
            CGContextRef context = UIGraphicsGetCurrentContext();
            context = UIGraphicsGetCurrentContext();
            [UIView beginAnimations:nil context:context];
            [UIView setAnimationDuration:0.75];
            btn.selected = TRUE;
            [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:btn cache:YES];
            [UIView commitAnimations];
            
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnBuyerSubscriptionClicked:(id)sender {
    @try
    {
        [SVProgressHUD dismiss];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:MSG_BUYSUBSCRIPTIONTITLE message:MSG_BUYSUBSCRIPTION delegate:self cancelButtonTitle:BUTTON_BUY otherButtonTitles:BUTTON_CANCEL,nil];
        alert.tag = 1;
        [alert show];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s --> %@",__FUNCTION__,exception.description);
    }
}

#pragma mark - Call Service
-(void)callDetailService
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
    
    aryData = nil;
    aryData = [[NSMutableArray alloc] init];
    
    detailService = DETAIL;
    NSMutableDictionary *dicParmeter = [[NSMutableDictionary alloc] init];
    [dicParmeter setValue:strAlbumId forKey:KEY_ALBUMID];
    [dicParmeter setValue:appDelegate.strCustomerID forKey:KEY_CUSTOMERID];
    
    [WebAPIRequest callWebSevice:self webServiceName:WS_ALBUMSDETAILS parameterDic:dicParmeter];
    dicParmeter = nil;
    
}
-(void)callViewService
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
    
    detailService = VIEW;
    NSMutableDictionary *dicParmeter = [[NSMutableDictionary alloc] init];
    [dicParmeter setValue:strAlbumId forKey:KEY_ALBUMID];
    [dicParmeter setValue:appDelegate.strCustomerID forKey:KEY_CUSTOMERID];
    
    [WebAPIRequest callWebSevice:self webServiceName:WS_ALBUMSVIEW parameterDic:dicParmeter];
    dicParmeter = nil;

}
-(void)callFavoriteService
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
    
    detailService = FAVORITE;
    NSMutableDictionary *dicParmeter = [[NSMutableDictionary alloc] init];
    [dicParmeter setValue:strAlbumId forKey:KEY_ALBUMID];
    [dicParmeter setValue:appDelegate.strCustomerID forKey:KEY_CUSTOMERID];
    
    [WebAPIRequest callWebSevice:self webServiceName:WS_ALBUMSFAVORITE parameterDic:dicParmeter];
    dicParmeter = nil;
    
}
#pragma mark - Web-Service Response Method
-(void)uploadRequestFinished:(ASIHTTPRequest *)request
{
    @try {
        [appDelegate endIgnoreEvent];
        [SVProgressHUD dismiss];
        NSString *responseString = [request responseString];
        
        NSDictionary *returnDict;
        if([responseString length])
        {
            returnDict = [responseString JSONValue];
        }
        
        if(returnDict)
        {
            if([[returnDict valueForKey:KEY_SUCCESS] intValue] == 0)
            {
                [SVProgressHUD showErrorWithStatus:[returnDict valueForKey:KEY_MSG]];
            }
            else
            {
                if(detailService == DETAIL)
                {
                    lblView.text = [NSString stringWithFormat:@"%@",[[[returnDict valueForKey:KEY_DATA] valueForKey:KEY_COUNT] valueForKey:KEY_VIEWCOUNT]];
                    
                    lblLike.text = [NSString stringWithFormat:@"%@",[[[returnDict valueForKey:KEY_DATA] valueForKey:KEY_COUNT] valueForKey:KEY_FAVOROTECOUNT]];
                    
                    if([[[[returnDict valueForKey:KEY_DATA] valueForKey:KEY_USERFLAG] valueForKey:KEY_FAVORITEBYUSER] intValue] == 1)
                    {
                        btnLike.selected = TRUE;
                    }
                    else
                    {
                        btnLike.selected = FALSE;
                    }
                    
                    
                    // Check wheather user like it or not
                    if([[[[returnDict valueForKey:KEY_DATA] valueForKey:KEY_USERFLAG] valueForKey:KEY_VIEWBYUSER] intValue] == 1)
                    {
                        btnView.selected = TRUE;
                        btnView.userInteractionEnabled = FALSE;
                    }
                    else
                    {
                        btnView.selected = FALSE;
                    }
                    
                    aryImage = nil;
                    aryImage = [[NSMutableArray alloc] init];
                    aryImage = [[[returnDict valueForKey:KEY_DATA] valueForKey:KEY_IMAGES] mutableCopy];
                    
                    for(int i=0;i<aryImage.count;i++)
                        [appDelegate storedImage:[[aryImage objectAtIndex:i] valueForKey:KEY_IMAGEURL]];

                    [carousel reloadData];
                    // End
                    
                    // Here we have to call View service is user is not View the album
                    if(btnView.selected == FALSE)
                    {
                        [self callViewService];
                    }
                    // End
                }
                else if(detailService == VIEW)
                {
                    appDelegate.isDetailChanged = TRUE;
                    btnView.selected = TRUE;
                    btnView.userInteractionEnabled = FALSE;
                    lblView.text = [NSString stringWithFormat:@"%d",[lblView.text intValue] + 1];

                }
                else if(detailService == FAVORITE)
                {
                    appDelegate.isDetailChanged = TRUE;
                    int count = [lblLike.text intValue];
                    if(btnLike.selected) // Already Like, we have to dislike it.
                    {
                        count --;
                    }
                    else  // other wise we have to like it.
                    {
                        count ++;
                    }
                    lblLike.text = [appDelegate convertIntoKserias:count];
                    btnLike.selected = !btnLike.selected;
                }
                
            }
        }
        responseString = nil;
        returnDict = nil;
        
    }
    @catch (NSException *exception) {
        NSLog(@"%s ==> %@",__FUNCTION__,exception.description);
    }
}
- (void)uploadRequestFailed:(ASIHTTPRequest *)request
{
    [appDelegate endIgnoreEvent];
    [SVProgressHUD showErrorWithStatus:MSG_SERVERERROR];
}

@end
