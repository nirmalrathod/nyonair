//
//  DetailViewController.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "CMSCoinView.h"

typedef enum {
    VIEW,
    FAVORITE,
    DETAIL
} DETAIL_SERVICE;


@interface DetailViewController : UIViewController<iCarouselDataSource, iCarouselDelegate,UIAlertViewDelegate>
{
    
    DETAIL_SERVICE detailService;
    IBOutlet HLayoutView *hLayout;
    IBOutlet iCarousel *carousel;
    
    IBOutlet UIButton *btnView;
    IBOutlet UIButton *btnLike;
    IBOutlet UIButton *btnBuy;
    NSString *strTitle;
    IBOutlet UIView *viewBuyNow;
    IBOutlet UIView *viewPrice;
    
    IBOutlet UILabel *lblView, *lblLike;
    
    NSString *strAlbumId;
    NSMutableArray *aryData,*aryImage;
    
}
@property (nonatomic, strong)NSString *strAlbumId;
@property (nonatomic, strong)NSString *strTitle;
@property (nonatomic, retain) IBOutlet iCarousel *carousel;
- (IBAction)btnViewClicked:(id)sender;
- (IBAction)btnLikeClicked:(id)sender;
- (IBAction)btnBuyNowClicked:(id)sender;
- (IBAction)btnBuyerSubscriptionClicked:(id)sender;

@property (nonatomic, retain) IBOutlet CMSCoinView *coinView;

@end
