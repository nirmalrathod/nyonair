//
//  ProfileViewController.m
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 5/7/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    // Set Method for Bar Button and left menu
    [self setupMenuBarButtonItems];
    // End
    
    self.title = TITLE_PROFILE;
    
    UIColor *colorPlaceholder = [UIColor colorWithRed:174.0/255.0 green:182.0/255.0 blue:186.0/255.0 alpha:1.0];

    
    [txtRegEmail setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];
    [txtRegFirstName setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];
    [txtRegLastName setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];
    [txtRegPassword setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];
    [txtRegNewPassword setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];
    [txtRegRetypePassword setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];

    
    txtRegEmail.inputAccessoryView = viewAccessory;
    txtRegFirstName.inputAccessoryView = viewAccessory;
    txtRegLastName.inputAccessoryView = viewAccessory;
    txtRegPassword.inputAccessoryView = viewAccessory;
    txtRegNewPassword.inputAccessoryView = viewAccessory;
    txtRegRetypePassword.inputAccessoryView = viewAccessory;

    
//    txtRegEmail.text = @"john@nyonair.com";
//    txtRegFirstName.text = @"John";
//    txtRegLastName.text = @"William";
//    txtRegPassword.text = @"john";


    viewInnerView.hidden = TRUE;
    viewPassword.hidden = TRUE;

    // Call the service for Getting User Data
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];

    currentService = USERDETAIL;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:appDelegate.strCustomerID forKey:KEY_CUSTOMERID];
    [WebAPIRequest callWebSevice:self webServiceName:WS_CUSTOMERDETAILS parameterDic:dic];
    dic = nil;
    // End
    
//    viewInnerView.hidden = FALSE;
//    viewPassword.hidden = TRUE;
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    appDelegate.lastController = viewProfile;
    [super viewWillAppear:animated];
}
#pragma mark - UIBarButtonItems

- (void)setupMenuBarButtonItems {
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
        self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    } else {
        self.navigationItem.leftBarButtonItem = nil;//[self leftMenuBarButtonItem];
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:IMAGE_MENU style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(leftSideMenuButtonPressed:)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 22.0f, 15.0f)];
    [a1 addTarget:self action:@selector(rightSideMenuButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_MENU forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}

- (UIBarButtonItem *)backBarButtonItem {
    
    
    UIButton *a1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [a1 setFrame:CGRectMake(0.0f, 0.0f, 20.0, 20.0f)];
    [a1 setImageEdgeInsets:UIEdgeInsetsMake(0, -9, 0, 0)];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
        a1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [a1 addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [a1 setImage:IMAGE_BACK forState:UIControlStateNormal];
    UIBarButtonItem *random = [[UIBarButtonItem alloc] initWithCustomView:a1];
    
    return random;
}
#pragma mark - Textfield Delegate Method
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
    
    barbtnNext.enabled = TRUE;
    barbtnPrevious.enabled = TRUE;
    float yPosition = 0.0;
    
    if(!appDelegate.isiPhone5)
    {
        if(textField == txtRegEmail || textField == txtRegPassword)
        {
            yPosition = 0;
            barbtnPrevious.enabled = FALSE;
        }
        else if(textField == txtRegFirstName || textField == txtRegNewPassword)
        {
            yPosition = 0;
            
        }
        else if(textField == txtRegLastName || textField == txtRegRetypePassword)
        {
            barbtnNext.enabled = FALSE;
            yPosition = -29;
        }
//        else if(textField == txtRegPassword)
//        {
//            barbtnNext.enabled = FALSE;
//            yPosition = -80;
//        }
    }
    
    if(viewInnerView.hidden == FALSE)
    {
        if(textField == txtRegEmail)
        {
            barbtnPrevious.enabled = FALSE;
        }
        else if(textField == txtRegLastName)
        {
            barbtnNext.enabled = FALSE;
        }
    }
    else
    {
        if(textField == txtRegPassword)
        {
            barbtnPrevious.enabled = FALSE;
        }
        else if(textField == txtRegRetypePassword)
        {
            barbtnNext.enabled = FALSE;
        }
    }

    [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                     animations:^{
                         
                         if(viewInnerView.hidden)
                             viewPassword.frame = CGRectMake(0, yPosition, self.view.frame.size.width, self.view.frame.size.height);
                        else
                            viewInnerView.frame = CGRectMake(0, yPosition, self.view.frame.size.width, self.view.frame.size.height);
                         
                     }
                     completion:^(BOOL finished){
                     }];
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self btnDoneNext:nil];
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - UIBarButtonItem Callbacks

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}

- (void)rightSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}



#pragma mark - Accssory View's Method
- (IBAction)btnDoneNext:(id)sender
{
    [activeTextField resignFirstResponder];
    [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                     animations:^{
                         
                         if(viewInnerView.hidden)
                             viewPassword.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                         else
                             viewInnerView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                         
                     }
                     completion:^(BOOL finished){
                     }];
    
}

- (IBAction)barBtnNext:(id)sender
{
    if(viewPassword.hidden == TRUE)
    {
        if(txtRegEmail.isFirstResponder)
            [txtRegFirstName becomeFirstResponder];
        else if(txtRegFirstName.isFirstResponder)
            [txtRegLastName becomeFirstResponder];
//        else if(txtRegLastName.isFirstResponder)
//            [txtRegPassword becomeFirstResponder];
    }
    else
    {
        if(txtRegPassword.isFirstResponder)
            [txtRegNewPassword becomeFirstResponder];
        else if(txtRegNewPassword.isFirstResponder)
            [txtRegRetypePassword becomeFirstResponder];
    }
}

- (IBAction)barBtnPrevious:(id)sender
{
    if(viewPassword.hidden == TRUE)
    {
        //        if(txtRegPassword.isFirstResponder)
        //            [txtRegLastName becomeFirstResponder];
        if(txtRegLastName.isFirstResponder)
            [txtRegFirstName becomeFirstResponder];
        else if(txtRegFirstName.isFirstResponder)
            [txtRegEmail becomeFirstResponder];
    }
    else
    {
        if([txtRegRetypePassword isFirstResponder])
            [txtRegNewPassword becomeFirstResponder];
        else if(txtRegNewPassword.isFirstResponder)
            [txtRegPassword becomeFirstResponder];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Button Method
- (IBAction)btnDoneUpdatePassword:(id)sender
{
    @try
    {
        [activeTextField resignFirstResponder];
        NSString *strCurrentPassword = [txtRegPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *strNewPassword = [txtRegNewPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *strRetypePassword = [txtRegRetypePassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        BOOL isUpdate = FALSE;
        
        if(strCurrentPassword.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTERPASSWORD];
        }
        else if(strNewPassword.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTERPASSWORDNEW];
        }
        else if(strRetypePassword.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTERPASSWORDRETYPE];
        }
        else if(![strNewPassword isEqualToString:strRetypePassword])
        {
            [SVProgressHUD showErrorWithStatus:MSG_PASSWORDNOTMATCH];
        }
        else
        {
            isUpdate = TRUE;
        }
        
        if(isUpdate)
        {
            // Call the service for Update Password
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];

            currentService = UPDATEPASSWORD;
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setValue:appDelegate.strCustomerID forKey:KEY_CUSTOMERID];
            [dic setValue:strCurrentPassword forKey:KEY_OLDPASSWORD];
            [dic setValue:strNewPassword forKey:KEY_NEWPASSWORD];
            [WebAPIRequest callWebSevice:self webServiceName:WS_CHANGEPASSWORD parameterDic:dic];
            dic = nil;
            // End

            
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception in %s with detail %@",__FUNCTION__,exception.description);
    }
}

- (IBAction)btnUpdatePasswordClicked:(id)sender
{
    @try
    {
        [UIView animateWithDuration:.50f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                         animations:^{
                             self.title = TITLE_UPDATEPROFILE;
                         }
                         completion:^(BOOL finished){
                         }];


        [viewPassword slideInFrom:kFTAnimationBottom duration:0.5 delegate:nil];
        [viewInnerView fadeOut:0.5 delegate:nil];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception in %s with detail %@",__FUNCTION__,exception.description);
    }
}

- (IBAction)btnCloseUpdatePasswordClicked:(id)sender
{
    
    [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                     animations:^{
                     }
                     completion:^(BOOL finished){
                         self.title = TITLE_PROFILE;

                     }];


//    [viewInnerView slideInFrom:kFTAnimationTop duration:0.5 delegate:nil];
//    [viewPassword fadeOut:0.5 delegate:nil];
    
    [viewPassword slideOutTo:kFTAnimationBottom duration:0.5 delegate:nil];
    [viewInnerView fadeIn:0.5 delegate:nil];

}

- (IBAction)btnUpdateProfileClicked:(id)sender
{
    @try
    {
        
        
        
        NSString *strEmail = [txtRegEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *strFirstName = [txtRegFirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *strLastName = [txtRegLastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        
        BOOL isSuccess = FALSE;
        
        if([strEmail isEqualToString:[dicUserDetail valueForKey:KEY_EMAIL]] && [strFirstName isEqualToString:[dicUserDetail valueForKey:KEY_FIRSTNAME]] && [strLastName isEqualToString:[dicUserDetail valueForKey:KEY_LASTNAME]])
        {
            [SVProgressHUD showErrorWithStatus:MES_NOCHANGES];
        }
        else if(strEmail.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTEREMAIL];
        }
        else if(![StringValidation validateEmail:strEmail])
        {
            [SVProgressHUD showErrorWithStatus:MSG_INVALIDEMAIL];
        }
        else if(strFirstName.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTERFIRSTNAME];
        }
        else if(strLastName.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTERLASTNAME];
        }
        else
        {
            isSuccess = TRUE;
        }
        if(isSuccess)
        {
//            [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
//            [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(moveToHomeScreen) userInfo:nil repeats:NO];

            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
            
            // Call the service for Update Password
            currentService = UDPATEPROFILE;
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setValue:appDelegate.strCustomerID forKey:KEY_CUSTOMERID];
            [dic setValue:strFirstName forKey:KEY_FIRSTNAME];
            [dic setValue:strLastName forKey:KEY_LASTNAME];
            [dic setValue:strEmail forKey:KEY_EMAIL];
            [WebAPIRequest callWebSevice:self webServiceName:WS_UDPATEDUSERDETAIL parameterDic:dic];
            dic = nil;
            // End

            
            
            
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}
-(void)moveToHomeScreen
{
    [SVProgressHUD dismiss];
    HomeViewController *demoController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    demoController.title = [NSString stringWithFormat:TITLE_PHOTO];
    
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:demoController];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

}

#pragma mark - Web-Service Response Method
-(void)uploadRequestFinished:(ASIHTTPRequest *)request
{
    [appDelegate endIgnoreEvent];
    [SVProgressHUD dismiss];
    NSString *responseString = [request responseString];
    
    NSDictionary *returnDict;
    if([responseString length])
    {
        returnDict = [responseString JSONValue];
    }
    
    if(returnDict)
    {
        if([[returnDict valueForKey:KEY_SUCCESS] intValue] == 0)
        {
            [SVProgressHUD showErrorWithStatus:[returnDict valueForKey:KEY_MSG]];
        }
        else
        {
            if(currentService == USERDETAIL)
            {
                viewInnerView.hidden = FALSE;
                viewPassword.hidden = TRUE;
                
                dicUserDetail = nil;
                dicUserDetail = [[NSMutableDictionary alloc] init];
                dicUserDetail = [[returnDict valueForKey:KEY_DATA] mutableCopy];
                
                txtRegEmail.text = [NSString stringWithFormat:@"%@",[[returnDict valueForKey:KEY_DATA] valueForKey:KEY_EMAIL]];
                
                txtRegFirstName.text = [NSString stringWithFormat:@"%@",[[returnDict valueForKey:KEY_DATA] valueForKey:KEY_FIRSTNAME]];
                
                txtRegLastName.text = [NSString stringWithFormat:@"%@",[[returnDict valueForKey:KEY_DATA] valueForKey:KEY_LASTNAME]];
            }
            else if(currentService == UPDATEPASSWORD)
            {

                [UIView animateWithDuration:.50f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                                 animations:^{
                                     self.title = TITLE_PROFILE;
                                 }
                                 completion:^(BOOL finished){
                                 }];

                [viewInnerView slideInFrom:kFTAnimationBottom duration:0.5 delegate:nil];
                [viewPassword fadeOut:0.5 delegate:nil];

            }
            else if(currentService == UDPATEPROFILE)
            {
                [self moveToHomeScreen];
            }
        }
    }
    
    
    responseString = nil;
    returnDict = nil;
    
}
- (void)uploadRequestFailed:(ASIHTTPRequest *)request
{
    [appDelegate endIgnoreEvent];
    [SVProgressHUD showErrorWithStatus:MSG_SERVERERROR];
}
@end
