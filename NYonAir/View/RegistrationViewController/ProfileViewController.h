//
//  ProfileViewController.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 5/7/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"

typedef enum {
    USERDETAIL,
    UPDATEPASSWORD,
    UDPATEPROFILE
} PROFILE_SERVICE;


@interface ProfileViewController : UIViewController<UITextFieldDelegate>
{
    PROFILE_SERVICE currentService;
    IBOutlet UITextField *txtRegEmail, *txtRegFirstName, *txtRegLastName;
    
    IBOutlet UITextField *txtRegPassword, *txtRegNewPassword, *txtRegRetypePassword;

    IBOutlet UIView *viewPassword;
    
    IBOutlet UIView *viewAccessory, *viewInnerView;
    NSMutableDictionary *dicUserDetail;
    UITextField *activeTextField;
    IBOutlet UIBarButtonItem *barbtnPrevious;
    IBOutlet UIBarButtonItem *barbtnDone;
    IBOutlet UIBarButtonItem *barbtnNext;
}
- (IBAction)btnDoneUpdatePassword:(id)sender;
- (IBAction)btnUpdatePasswordClicked:(id)sender;
- (IBAction)btnCloseUpdatePasswordClicked:(id)sender;
- (IBAction)btnUpdateProfileClicked:(id)sender;
@end
