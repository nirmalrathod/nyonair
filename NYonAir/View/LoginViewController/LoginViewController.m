//
//  LoginViewController.m
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import "LoginViewController.h"
#define bottomHeightForAnimation 81
@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    
    UIColor *colorPlaceholder = [UIColor colorWithRed:174.0/255.0 green:182.0/255.0 blue:186.0/255.0 alpha:1.0];
    
    [txtLoginEmail setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];
    [txtLoginPassword setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];

    [txtRegEmail setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];
    [txtRegFirstName setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];
    [txtRegLastName setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];
    [txtRegPassword setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];

    [txtForgotPassword setValue:colorPlaceholder forKeyPath:@"_placeholderLabel.textColor"];

    
    txtLoginEmail.inputAccessoryView = viewAccessory;
    txtLoginPassword.inputAccessoryView = viewAccessory;
    txtRegEmail.inputAccessoryView = viewAccessory;
    txtRegFirstName.inputAccessoryView = viewAccessory;
    txtRegLastName.inputAccessoryView = viewAccessory;
    txtRegPassword.inputAccessoryView = viewAccessory;
    
//    [self addObserverIntoView];
    self.navigationController.navigationBarHidden = TRUE;
     imgBackground.autoresizingMask = UIViewAutoresizingFlexibleHeight;

    
    imgBackground.image = [imgBackground.image stretchableImageWithLeftCapWidth:0 topCapHeight:60];

    // Here we set the Original and Center Frame for Logo
    logoOriginalFrame = imgLogo.frame;
    
    logoCenterFrame = CGRectMake(imgLogo.frame.origin.x, self.view.frame.size.height/2 - imgLogo.frame.size.height/2, imgLogo.frame.size.width, imgLogo.frame.size.height);
    // End
    
    rectForHiddenView = CGRectMake(0, appDelegate.window.frame.size.height, 320, viewLogin.frame.size.height);
    
    rectForVisibleView = viewLogin.frame;
    
    imageOriginalFrame = imgBackground.frame;
    
//    viewRegister.frame = rectForHiddenView;
    viewRegister.hidden = TRUE;
    viewForgotPassword.hidden = TRUE;

    isLoginView = TRUE;
    btnClose.hidden = TRUE;

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    txtLoginEmail.text = @"nirmalsinh@creolestudios.com";
    txtLoginPassword.text = @"nirmit";
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }

    [super viewWillDisappear:animated];
}
-(void)addObserverIntoView
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

}
-(void)removeObserverFromView
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - Textfield Delegate Method
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
    
    barbtnNext.enabled = TRUE;
    barbtnPrevious.enabled = TRUE;
    float yPosition = 0.0;

    if(isLoginView)
    {
        if(textField == txtLoginEmail)
        {
            //yPosition = -15;
            barbtnPrevious.enabled = FALSE;
        }
        else
        {
            barbtnNext.enabled = FALSE;
        }
        yPosition = -66;

    }
    else
    {
        if(textField == txtRegEmail)
        {
            yPosition = -15;
            barbtnPrevious.enabled = FALSE;
        }
        else if(textField == txtRegFirstName)
        {
            yPosition = -70;

        }
        else if(textField == txtRegLastName)
        {
            yPosition = -125;
        }
        else if(textField == txtRegPassword)
        {
            barbtnNext.enabled = FALSE;
            yPosition = -180;
        }
    }
    
    if(!appDelegate.isiPhone5)
        yPosition -= 20;
    
    [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                     animations:^{
                         
                         self.view.frame = CGRectMake(0, yPosition, self.view.frame.size.width, self.view.frame.size.height);
                         
                     }
                     completion:^(BOOL finished){
                     }];

    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self btnDoneNext:nil];
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Web-Service Response Method
-(void)uploadRequestFinished:(ASIHTTPRequest *)request
{
    [appDelegate endIgnoreEvent];
    [SVProgressHUD dismiss];
    NSString *responseString = [request responseString];
    
    NSDictionary *returnDict;
    if([responseString length])
    {
        returnDict = [responseString JSONValue];
    }
    
    if(returnDict)
    {
        if([[returnDict valueForKey:KEY_SUCCESS] intValue] == 0)
        {
            [SVProgressHUD showErrorWithStatus:[returnDict valueForKey:KEY_MSG]];
        }
        else
        {
            if(currentService == LOGIN)
            {
                appDelegate.strCustomerID = nil;
                appDelegate.strCustomerID = [NSString stringWithFormat:@"%@",[[returnDict valueForKey:KEY_DATA]valueForKey:KEY_CUSTOMERID]];
                
                [[NSUserDefaults standardUserDefaults] setObject:[returnDict valueForKey:KEY_DATA] forKey:@"USER_DATA"];
                [[NSUserDefaults standardUserDefaults] synchronize];

//                appDelegate.strCustomerID = @"1";
                [appDelegate addLeftMenu];
            }
            else if(currentService == FORGOTPASSWORD)
            {
                isLoginView = TRUE;
                [self animatedSignup];
            }
            else if(currentService == REGISTRATION)
            {
                [[NSUserDefaults standardUserDefaults] setObject:[returnDict valueForKey:KEY_DATA] forKey:@"USER_DATA"];
                [[NSUserDefaults standardUserDefaults] synchronize];

//                [self closeRegistrationView];
                appDelegate.strCustomerID = nil;
                appDelegate.strCustomerID = [NSString stringWithFormat:@"%@",[[returnDict valueForKey:KEY_DATA]valueForKey:KEY_CUSTOMERID]];

                [appDelegate addLeftMenu];

            }
        }
    }
    
    
    responseString = nil;
    returnDict = nil;
    
}
- (void)uploadRequestFailed:(ASIHTTPRequest *)request
{
    [SVProgressHUD showErrorWithStatus:MSG_SERVERERROR];
    [appDelegate endIgnoreEvent];

}
#pragma mark - Notification Method
- (void)keyboardWasShown:(NSNotification *)notification
{
    [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                     animations:^{
                         
                         float yPosition;
                         if(isLoginView)
                         {
                             if(appDelegate.isiPhone5)
                                 yPosition = -90;
                             else
                                 yPosition = -110;
                         }
                         else
                         {
                             if(appDelegate.isiPhone5)
                                 yPosition = -200;
                             else
                                 yPosition = -160;

                         }
                         
                             self.view.frame = CGRectMake(0, yPosition, self.view.frame.size.width, self.view.frame.size.height);

                     }
                     completion:^(BOOL finished){
                     }];
    

}
-(void)keyboardWillHide:(NSNotification *)aNotification
{
    [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                     animations:^{
                         self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                         
                     }
                     completion:^(BOOL finished){
                     }];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Method
- (IBAction)btnSubmitForgotPasswordClicked:(id)sender {
    @try
    {
        NSString *strEmail = [txtForgotPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        
        BOOL isSuccess = TRUE;
        
        if(strEmail.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTEREMAIL];
            isSuccess = FALSE;
        }
        else if(![StringValidation validateEmail:strEmail])
        {
            [SVProgressHUD showErrorWithStatus:MSG_INVALIDEMAIL];
            isSuccess = FALSE;
        }
        else
        {
            isSuccess = TRUE;
        }
        
        if(isSuccess)
        {
            [activeTextField resignFirstResponder];
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
            
            currentService = FORGOTPASSWORD;
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setValue:strEmail forKey:KEY_EMAIL];
            [WebAPIRequest callWebSevice:self webServiceName:WS_FORGOTPASSWORD parameterDic:dic];
            dic = nil;
        }
        
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnLoginRegistration:(id)sender
{
    @try
    {
        
        [appDelegate beginIgnoreEvent];
        txtRegEmail.text=@"";
        txtRegFirstName.text=@"";
        txtRegLastName.text=@"";
        txtRegPassword.text=@"";
        isLoginView = FALSE;
        //    btnClose.hidden = FALSE;
        
        [self animatedRegistration];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnDoneMethod:(id)sender
{
    @try
    {
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)segChangeClicked:(id)sender
{
    @try
    {
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnRegisterClicked:(id)sender
{
    @try
    {
        
        NSString *strEmail = [txtRegEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *strFirstName = [txtRegFirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *strLastName = [txtRegLastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *strPassword = [txtRegPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        BOOL isSuccess = FALSE;
        if(strEmail.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTEREMAIL];
        }
        else if(![StringValidation validateEmail:strEmail])
        {
            [SVProgressHUD showErrorWithStatus:MSG_INVALIDEMAIL];
        }
        else if(strFirstName.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTERFIRSTNAME];
        }
        else if(strLastName.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTERLASTNAME];
        }
        else if(strPassword.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTERPASSWORD];
        }
        else
        {
            isSuccess = TRUE;
        }
        if(isSuccess)
        {
            [activeTextField resignFirstResponder];
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
            
            currentService = REGISTRATION;
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setValue:strEmail forKey:KEY_EMAIL];
            [dic setValue:strPassword forKey:KEY_PASSWORD];
            [dic setValue:strFirstName forKey:KEY_FIRSTNAME];
            [dic setValue:strLastName forKey:KEY_LASTNAME];
            
            [WebAPIRequest callWebSevice:self webServiceName:WS_REGISTER parameterDic:dic];
            dic = nil;

//            [self closeRegistrationView];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

-(void)closeFogotPasswordView
{
    @try
    {
        [appDelegate beginIgnoreEvent];
        txtLoginEmail.text = @"";
        txtLoginPassword.text = @"";
        isLoginView = TRUE;
        [self animatedSignup];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

-(void)closeRegistrationView
{
    @try
    {
        [appDelegate beginIgnoreEvent];
        txtLoginEmail.text = @"";
        txtLoginPassword.text = @"";
        isLoginView = TRUE;
        [self animatedSignup];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnLoginClicked:(id)sender
{
    @try
    {
//        [appDelegate fetchAvailableProducts];
//        return;
        NSString *strEmail = [txtLoginEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *strPassword = [txtLoginPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        BOOL isSuccess = TRUE;
        
        if(strEmail.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTEREMAIL];
            isSuccess = FALSE;
        }
        else if(![StringValidation validateEmail:strEmail])
        {
            [SVProgressHUD showErrorWithStatus:MSG_INVALIDEMAIL];
            isSuccess = FALSE;
        }
        else if(strPassword.length == 0)
        {
            [SVProgressHUD showErrorWithStatus:MSG_ENTERPASSWORD];
            isSuccess = FALSE;
        }
        else
        {
            isSuccess = TRUE;
        }
        
        if(isSuccess)
        {
            [activeTextField resignFirstResponder];
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
            
            NSMutableDictionary *dicParmeter = [[NSMutableDictionary alloc] init];
            [dicParmeter setValue:strEmail forKey:KEY_EMAIL];
            [dicParmeter setValue:strPassword forKey:KEY_PASSWORD];
            
            currentService = LOGIN;
            [WebAPIRequest callWebSevice:self webServiceName:WS_LOGIN parameterDic:dicParmeter];
            dicParmeter = nil;
        }
        
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnForgotPasswordClicked:(id)sender
{
    @try
    {
        
            [appDelegate beginIgnoreEvent];
            txtForgotPassword.text=@"";
            isLoginView = FALSE;
            //    btnClose.hidden = FALSE;
            
            [self animatedForgotPassword];

//        [self removeObserverFromView];
//        alertForgotPassword = nil;
//        alertForgotPassword = [[UIAlertView alloc] initWithTitle:@"Forgot Password" message:@"" delegate:self cancelButtonTitle:@"Send"  otherButtonTitles:@"Cancel",nil];
//        alertForgotPassword.alertViewStyle = UIAlertViewStylePlainTextInput;
//        
//        UITextField* answerField = [alertForgotPassword textFieldAtIndex:0];
//        answerField.keyboardType = UIKeyboardTypeEmailAddress;
//        answerField.placeholder = @"Enter your email address";
//        
//        [alertForgotPassword show];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}


- (IBAction)btnClosedClicked:(id)sender
{
    @try
    {
        [activeTextField resignFirstResponder];
        if(viewForgotPassword.hidden == TRUE)
            [self closeRegistrationView];
        else
            [self closeFogotPasswordView];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    @try
    {
        
        [self addObserverIntoView];
        if(buttonIndex == 0)
        {
            NSString *strEmailAddress = [alertForgotPassword textFieldAtIndex:0].text;
            strEmailAddress = [strEmailAddress stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (strEmailAddress.length == 0)
            {
                [SVProgressHUD showErrorWithStatus:MSG_ENTEREMAIL];
                
            }
            else if(![StringValidation validateEmail:strEmailAddress])
            {
                [SVProgressHUD showErrorWithStatus:MSG_INVALIDEMAIL];
            }
            else
            {
                // Success
                // Call the forgot password service
            }
        }
        else
        {
            // Cancel Button
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

#pragma mark - Animation Method

-(void)getRegisterView
{
    viewRegister.hidden = FALSE;
    [viewRegister slideInFrom:kFTAnimationBottom duration:0.4 delegate:nil];
}

-(void)animatedRegistration
{
    @try
    {
        //    [viewLogin slideOutTo:kFTAnimationBottom duration:ANIMATION_TIME delegate:self startSelector:nil stopSelector:@selector(getRegisterView)];
        
        [UIView animateWithDuration:ANIMATION_TIME delay:0.0f options:(UIViewAnimationOptionCurveEaseIn)
                         animations:^{
                             
                             
                             //                         imgBackground.frame = CGRectMake(0, 0, 320, self.view.frame.size.height+bottomHeightForAnimation);
                             imgBackground.frame = CGRectMake(0, 0, 320, appDelegate.window.frame.size.height+bottomHeightForAnimation);
                             
                             //viewLogin.frame = rectForHiddenView;
                             //                         viewLogin.frame = CGRectMake(0, self.view.frame.size.height, viewLogin.frame.size.width, viewLogin.frame.size.height);
                             imgBackground.image = [imgBackground.image stretchableImageWithLeftCapWidth:0 topCapHeight:60];
                             
                             imgLogo.frame = logoCenterFrame;
                         }
                         completion:^(BOOL finished){
                             
                             [UIView animateWithDuration:ANIMATION_TIME delay:0.0f options:(UIViewAnimationOptionCurveEaseInOut)
                                              animations:^{
                                                  
                                                  CGRect imgRect;
                                                  if(appDelegate.isiPhone5)
                                                      imgRect = CGRectMake(0, 0, 320,appDelegate.window.frame.size.height - viewLogin.frame.size.height+bottomHeightForAnimation);
                                                  else
                                                      imgRect = CGRectMake(0, 62, 320, appDelegate.window.frame.size.height - viewLogin.frame.size.height+bottomHeightForAnimation);
                                                  imgBackground.frame = imageOriginalFrame;
                                                  
                                                  imgLogo.frame = logoOriginalFrame;
                                                  //viewRegister.frame = rectForVisibleView;
                                                  
                                                  viewRegister.hidden = FALSE;
                                                  viewLogin.hidden = TRUE;
                                                  [btnClose fadeIn:ANIMATION_TIME delegate:self];
                                                  //                                              CGRect rectRegistration = viewRegister.frame;
                                                  //                                              rectRegistration.origin.y = self.view.frame.size.height - rectRegistration.size.height;
                                                  //
                                                  //                                              viewRegister.frame = rectRegistration;
                                                  //
                                              }
                                              completion:^(BOOL finished){
                                                  
                                                          [appDelegate endIgnoreEvent];
                                                  
                                              }];
                         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}
-(void)animatedForgotPassword
{
    @try
    {
        //    [viewLogin slideOutTo:kFTAnimationBottom duration:ANIMATION_TIME delegate:self startSelector:nil stopSelector:@selector(getRegisterView)];
        
        [UIView animateWithDuration:ANIMATION_TIME delay:0.0f options:(UIViewAnimationOptionCurveEaseIn)
                         animations:^{
                             
                             imgBackground.frame = CGRectMake(0, 0, 320, appDelegate.window.frame.size.height+bottomHeightForAnimation);
                             
                             imgBackground.image = [imgBackground.image stretchableImageWithLeftCapWidth:0 topCapHeight:60];
                             
                             imgLogo.frame = logoCenterFrame;
                         }
                         completion:^(BOOL finished){
                             
                             [UIView animateWithDuration:ANIMATION_TIME delay:0.0f options:(UIViewAnimationOptionCurveEaseInOut)
                                              animations:^{
                                                  
                                                  CGRect imgRect;
                                                  if(appDelegate.isiPhone5)
                                                      imgRect = CGRectMake(0, 0, 320,appDelegate.window.frame.size.height - viewLogin.frame.size.height+bottomHeightForAnimation);
                                                  else
                                                      imgRect = CGRectMake(0, 62, 320, appDelegate.window.frame.size.height - viewLogin.frame.size.height+bottomHeightForAnimation);
                                                  imgBackground.frame = imageOriginalFrame;
                                                  
                                                  imgLogo.frame = logoOriginalFrame;
                                                  
                                                  viewForgotPassword.hidden = FALSE;
                                                  viewLogin.hidden = TRUE;
                                                  [btnClose fadeIn:ANIMATION_TIME delegate:self];
                                              }
                                              completion:^(BOOL finished){
                                                  
                                                  [appDelegate endIgnoreEvent];
                                                  
                                              }];
                         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

-(void)getLoginView
{
    @try
    {
        viewLogin.hidden = FALSE;
        [viewLogin slideInFrom:kFTAnimationBottom duration:0.4 delegate:nil];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}


-(void)animatedSignup
{
    @try
    {
        
        //    return;
        //    [viewRegister slideOutTo:kFTAnimationBottom duration:ANIMATION_TIME delegate:self startSelector:nil stopSelector:@selector(getLoginView)];
        
        [UIView animateWithDuration:0.55 delay:0.0f options:(UIViewAnimationOptionCurveEaseIn)
                         animations:^{
                             imgBackground.frame = CGRectMake(0, 0, 320, appDelegate.window.frame.size.height+bottomHeightForAnimation);
                             //                         viewRegister.frame = CGRectMake(0, self.view.frame.size.height, viewRegister.frame.size.width, viewRegister.frame.size.height);
                             imgBackground.image = [imgBackground.image stretchableImageWithLeftCapWidth:0 topCapHeight:60];
                            // viewRegister.frame = rectForHiddenView;
                             
                             imgLogo.frame = logoCenterFrame;
                             
                         }
                         completion:^(BOOL finished){
                             
                             [UIView animateWithDuration:ANIMATION_TIME delay:0.0f options:(UIViewAnimationOptionCurveEaseInOut)
                                              animations:^{
                                                  
                                                  CGRect imgRect;
                                                  if(appDelegate.isiPhone5)
                                                      imgRect = CGRectMake(0, 0, 320,appDelegate.window.frame.size.height - viewLogin.frame.size.height + bottomHeightForAnimation);
                                                  else
                                                      imgRect = CGRectMake(0, 62, 320, appDelegate.window.frame.size.height - viewLogin.frame.size.height + bottomHeightForAnimation);
                                                  imgBackground.frame = imageOriginalFrame;
                                                  
                                                  imgLogo.frame = logoOriginalFrame;
                                                  
                                                  //                                              btnClose.hidden = TRUE;
                                                  [btnClose fadeOut:ANIMATION_TIME delegate:self];
                                                  
                                                  //                                              CGRect rectRegistration = viewLogin.frame;
                                                  //                                              rectRegistration.origin.y = self.view.frame.size.height - rectRegistration.size.height;
                                                  //
                                                  //viewLogin.frame = rectForVisibleView;;
                                                  viewLogin.hidden = FALSE;
                                                  viewRegister.hidden = TRUE;
                                                  viewForgotPassword.hidden = TRUE;
                                                  
                                              }
                                              completion:^(BOOL finished){
                                                  
                                                         [appDelegate endIgnoreEvent];
                                                  
                                              }];
                         }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

- (IBAction)btnDoneNext:(id)sender
{
    [activeTextField resignFirstResponder];
    [UIView animateWithDuration:.25f delay:0.0f options:(UIViewAnimationOptionTransitionCurlUp)
                     animations:^{
                         
                         self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                         
                     }
                     completion:^(BOOL finished){
                     }];

}

- (IBAction)barBtnNext:(id)sender
{
    if(isLoginView)
    {
        if(txtLoginEmail.isFirstResponder)
            [txtLoginPassword becomeFirstResponder];
    }
    else
    {
        if(txtRegEmail.isFirstResponder)
            [txtRegFirstName becomeFirstResponder];
        else if(txtRegFirstName.isFirstResponder)
            [txtRegLastName becomeFirstResponder];
        else if(txtRegLastName.isFirstResponder)
            [txtRegPassword becomeFirstResponder];
    }
}

- (IBAction)barBtnPrevious:(id)sender
{
    if(isLoginView)
    {
        if(txtLoginPassword.isFirstResponder)
            [txtLoginEmail becomeFirstResponder];
    }
    else
    {
        if(txtRegPassword.isFirstResponder)
            [txtRegLastName becomeFirstResponder];
        else if(txtRegLastName.isFirstResponder)
            [txtRegFirstName becomeFirstResponder];
        else if(txtRegFirstName.isFirstResponder)
            [txtRegEmail becomeFirstResponder];
    }
}

@end
