//
//  LoginViewController.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    LOGIN,
    FORGOTPASSWORD,
    REGISTRATION
} CURRENT_SERVICE;

@interface LoginViewController : UIViewController <UITextFieldDelegate>
{
    CURRENT_SERVICE currentService;
    IBOutlet UISegmentedControl *segNextPrev;
    IBOutlet UITextField *txtLoginEmail, *txtLoginPassword;
    IBOutlet UITextField *txtRegEmail, *txtRegFirstName, *txtRegLastName, *txtRegPassword;
    IBOutlet UIImageView *imgLogo;
    IBOutlet UIButton *btnClose;
    
    IBOutlet UIView *viewAccessory;
    IBOutlet UIView *viewRegister;
    IBOutlet UIView *viewLogin;
    IBOutlet UIImageView *imgBackground;
    
    CGRect logoOriginalFrame, logoCenterFrame;
    CGRect imageOriginalFrame,rectForHiddenView,rectForVisibleView;
    
    BOOL isLoginView; // Yes --> Login view is visible, No --> register view is visible.
    
    UIAlertView *alertForgotPassword;
    
    IBOutlet UIView *viewForgotPassword;
    IBOutlet UITextField *txtForgotPassword;
    UITextField *activeTextField;
    IBOutlet UIBarButtonItem *barbtnPrevious;
    IBOutlet UIBarButtonItem *barbtnDone;
    IBOutlet UIBarButtonItem *barbtnNext;
    IBOutlet UITableView *tempTable;
}
- (IBAction)btnForgotPasswordClicked:(id)sender;
- (IBAction)btnLoginRegistration:(id)sender;
- (IBAction)btnDoneMethod:(id)sender;
- (IBAction)segChangeClicked:(id)sender;
- (IBAction)btnRegisterClicked:(id)sender;
- (IBAction)btnLoginClicked:(id)sender;
- (IBAction)btnSubmitForgotPasswordClicked:(id)sender;
- (IBAction)btnClosedClicked:(id)sender;
-(void)animatedRegistration;
-(void)animatedSignup;
- (IBAction)btnDoneNext:(id)sender;
- (IBAction)barBtnNext:(id)sender;
- (IBAction)barBtnPrevious:(id)sender;

@end
