//
//  AppDelegate.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	viewHomeScreen = 0,
	viewECard,
	viewNews,
    viewContact,
    viewSubscription,
    viewProfile,
    viewLogout
} LastVisibleController;


@interface AppDelegate : UIResponder <UIApplicationDelegate,SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
    Reachability* hostReach;
    Reachability* internetReach;
    
    BOOL isInternetAvailable;
    BOOL isiPhone5;
    
    UINavigationController *objNavigation;
    
    LastVisibleController lastController;
    NSString *strCustomerID;
    BOOL isDetailChanged; // This flag is TRUE when user call AlbumView or AlbumLike service. So In home screen, we have to call listing service again.
    
    // For InApp Purchase
    SKProductsRequest *productsRequest;
    NSArray *validProducts;
    // End
}
@property (nonatomic)BOOL isDetailChanged;
@property (nonatomic,strong)NSString *strCustomerID;
@property (nonatomic)LastVisibleController lastController;
@property  (strong, nonatomic) NSString *strLongitude;
@property (strong,nonatomic)UINavigationController *objNavigation;
@property (nonatomic)BOOL isiPhone5;
@property (nonatomic)BOOL isInternetAvailable;
@property (strong, nonatomic) UIWindow *window;

-(NSString *)convertIntoKserias :(int)convertNumber;
#pragma mark Reachability Method
- (void) reachabilityChanged: (NSNotification* )note;
- (void) updateInterfaceWithReachability: (Reachability*) curReach;

-(void)storedImage : (NSString *)strImageURL;

-(void)addLeftMenu;

-(void)beginIgnoreEvent;
-(void)endIgnoreEvent;

-(void)fetchAvailableProducts;
@end
