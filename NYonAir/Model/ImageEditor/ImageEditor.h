//
//  ImageEditor.h
//  ImageViewLabel
//
//  Created by Rajesh Ritesh on 15/05/14.
//  Copyright (c) 2014 Rajesh Ritesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageEditor : UIView



@property (nonatomic, retain) UIImage *image;

-(UIImage *)getOriginalImage;

@end
