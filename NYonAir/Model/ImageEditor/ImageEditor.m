//
//  ImageEditor.m
//  ImageViewLabel
//
//  Created by Rajesh Ritesh on 15/05/14.
//  Copyright (c) 2014 Rajesh Ritesh. All rights reserved.
//

#import "ImageEditor.h"

@interface ImageEditor ()
{
    float sizeFactor;
}
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImageView *imageViewOriginal;
//@property (nonatomic, strong) ;

@end

@implementation ImageEditor

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self makeView];
    }
    return self;
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self makeView];
    }
    return self;
}


-(void)makeView
{
    self.imageViewOriginal = [[UIImageView alloc] init];
    
    self.imageView = [[UIImageView alloc] init];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self addSubview:self.imageView];
    [self addSubview:self.imageViewOriginal];

}


#pragma mark - 


-(void)setImage:(UIImage *)image
{
    CGSize imageSize = image.size;
    
    CGRect frame;
    frame.origin = CGPointZero;
    frame.size = imageSize;
    self.imageViewOriginal.frame = frame;
    self.imageViewOriginal.image = image;
    self.imageViewOriginal.hidden = TRUE;
    
    CGSize selfSize = self.frame.size;
    
    if ((imageSize.width/imageSize.height) > (selfSize.width/selfSize.height))
    {
        sizeFactor = selfSize.width / imageSize.width;
        imageSize.height = imageSize.height * sizeFactor;
        imageSize.width = selfSize.width;
    }
    else
    {
        sizeFactor = selfSize.height / imageSize.height;
        imageSize.width = imageSize.width * sizeFactor;
        imageSize.height = selfSize.height;
    }
    
    frame.size = imageSize;
    self.imageView.frame = frame;
    self.imageView.image = image;
    
    
    [self sendSubviewToBack:self.imageViewOriginal];
    [self sendSubviewToBack:self.imageView];
    
//    self.imageView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    
}

-(UIImage *)getOriginalImage
{
    for (UILabel *lbl in self.subviews)
    {
        if ([lbl isKindOfClass:[UILabel class]])
        {
            CGRect frame = lbl.frame;
            
            frame.origin.x = frame.origin.x / sizeFactor;
            frame.origin.y = frame.origin.y / sizeFactor;
            
            frame.size.width = frame.size.width / sizeFactor;
            frame.size.height = frame.size.height / sizeFactor;
            
            UILabel *lbl2 = [[UILabel alloc] initWithFrame:frame];
            
            lbl2.backgroundColor = lbl.backgroundColor;
            lbl2.textColor       = lbl.textColor;
            lbl2.text            = lbl.text;
            lbl2.alpha           = lbl.alpha;
            lbl2.textAlignment   = lbl.textAlignment;
            lbl2.backgroundColor = lbl.backgroundColor;
            lbl2.font            = [UIFont fontWithName:lbl.font.fontName size:lbl.font.pointSize / sizeFactor];
            
            [self.imageViewOriginal addSubview:lbl2];
        }
    }
    
    
    self.imageViewOriginal.hidden = FALSE;

    UIImage *image = [self captureView:self.imageViewOriginal];

    self.imageViewOriginal.hidden = TRUE;
    
    return image;
}

- (UIImage *)captureView:(UIView *)view
{
    CGRect screenRect = view.bounds;
    
    UIGraphicsBeginImageContext(screenRect.size);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [[UIColor blackColor] set];
    CGContextFillRect(ctx, screenRect);
    
    [view.layer renderInContext:ctx];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();

    return newImage;
}












@end
