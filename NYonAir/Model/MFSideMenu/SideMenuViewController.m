//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import "SideMenuViewController.h"
#import "MFSideMenu.h"

//#import "NearbyViewController.h"
//#import "KTVViewController.h"
//#import "SignUpViewController.h"
//#import "EventViewController.h"
#import "HomeViewController.h"
#import "eCardViewController.h"
#import "NewsViewController.h"
#import "SharingViewController.h"

@implementation SideMenuViewController

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSString stringWithFormat:@"Section %d", section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"Item %d", indexPath.row];
    
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

 /*   if (indexPath.row == 0) // Home
    {
        HomeViewController *demoController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"Home"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

//    NearbyViewController *demoController = [[NearbyViewController alloc] initWithNibName:@"NearbyViewController" bundle:nil];
//    demoController.title = [NSString stringWithFormat:@"NearyBy"];
//    
//    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//    NSArray *controllers = [NSArray arrayWithObject:demoController];
//    navigationController.viewControllers = controllers;
//    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    else if(indexPath.row == 1)
    {
        KTVViewController *demoController = [[KTVViewController alloc] initWithNibName:@"KTVViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"KTV"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    else if(indexPath.row == 2)
    {
        SignUpViewController *demoController = [[SignUpViewController alloc] initWithNibName:@"SignUpViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"KTV"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    else if(indexPath.row == 3)
    {
        EventViewController *demoController = [[EventViewController alloc] initWithNibName:@"EventViewController" bundle:nil];
        demoController.title = [NSString stringWithFormat:@"KTV"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:demoController];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
*/
}


@end
