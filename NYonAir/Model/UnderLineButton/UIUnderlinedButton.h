//
//  UIUnderlinedButton.h
//  Oenobook
//
//  Created by Administrator on 15/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIUnderlinedButton : UIButton{
    
}
+ (UIUnderlinedButton*) underlinedButton;
@end
