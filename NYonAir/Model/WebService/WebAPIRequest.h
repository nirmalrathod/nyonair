

#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ASIFormDataRequest.h"


@interface WebAPIRequest : NSObject {
    
    ASINetworkQueue *networkQueue;
}


@property (nonatomic, retain) ASINetworkQueue *networkQueue;

+(void)callWebSevice :(NSObject *)delegate webServiceName : (NSString *)strWebServiceName parameterDic : (NSMutableDictionary *)dicData;
+(NSMutableDictionary *)callService : (NSMutableDictionary *)dicParams : (NSString *)strServiceName;
+(void)uploadImageToServer :(UIImage *)image;
@end
