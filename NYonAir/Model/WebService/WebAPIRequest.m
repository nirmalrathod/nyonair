
#import "WebAPIRequest.h"
#import "AppDelegate.h"
#import "SBJson.h"


@interface NSObject(Extended)
-(void) getAPIResponse :(NSDictionary *)items;
@end

@interface ConnectionClass:NSObject{
	NSMutableData *receivedData;
	NSString *className;
	NSString *rootName;
	NSObject *m_delegate;
	int tag;
}

//-(id)initWithClass:(NSString *)class withRoot:(NSString *)root withDelegate:(NSObject *)delegate withTag:(int)t;
-(id)initWithClass:(NSString *)class withRoot:(NSString *)root withDelegate:(NSObject *)delegate;
@end

@implementation ConnectionClass

-(id)initWithClass:(NSString *)class withRoot:(NSString *)root withDelegate:(NSObject *)delegate
{
 	m_delegate = delegate;
	className = class;
	rootName = root;
	return self;
}

- (void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data {
	if (receivedData != nil) {
		[receivedData appendData:data];
	} else {
		receivedData = [[NSMutableData alloc] initWithData:data];
	}
}

#pragma mark - Data did finish loading.
// recive data from server.

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	if([className length]==0) {
		NSMutableArray *item = [[NSMutableArray alloc] initWithObjects:rootName,receivedData,nil];
		//[m_delegate setData:@"" items:item withtag:tag];
		[item release];
	}
	else {
        
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        
        NSString *strData = [[NSString alloc]  initWithBytes:[receivedData bytes]
                                                      length:[receivedData length] encoding: NSUTF8StringEncoding];
        
        NSDictionary *JSON =
        [NSJSONSerialization JSONObjectWithData: [strData dataUsingEncoding:NSUTF8StringEncoding]
                                        options: NSJSONReadingMutableContainers
                                          error:nil];
        
      
        [m_delegate getAPIResponse:JSON];
        [strData release];
        [pool release];
	}
}

-(void)dealloc {
	[receivedData release];
	[super dealloc];
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [m_delegate getAPIResponse:nil];
}
@end

@implementation WebAPIRequest
@synthesize networkQueue;


#pragma mark -----------------
#pragma mark Block Service Call
+(NSMutableDictionary *)callService : (NSMutableDictionary *)dicParams : (NSString *)strServiceName
{
    
    
    NSArray *arr = [NSArray arrayWithObject:dicParams];
    NSData *data = [NSJSONSerialization dataWithJSONObject:arr options:kNilOptions error:nil];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    str = [NSString stringWithFormat:@"%@",str];
    data = [str dataUsingEncoding:(NSUTF8StringEncoding)];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASEURL,strServiceName];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/x-www-form-urlencoded " forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:data];
    [request setTimeoutInterval:100];
    
    NSError *err = nil;
    NSHTTPURLResponse *res = nil;
    NSData *retData = [NSURLConnection sendSynchronousRequest:request returningResponse:&res error:&err];
    
    
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [SVProgressHUD dismiss];
    if (err)
    {
        [SVProgressHUD showErrorWithStatus:@"Error"];
        return nil;
    }
    else
    {
        NSString *returnString = [[NSString alloc] initWithData:retData encoding:NSUTF8StringEncoding];
        NSMutableDictionary *dic = [returnString JSONValue];
        NSLog(@"Response : %@",[dic description]);
        NSLog(@"=================== Web Service Call End ===================");
        if([[dic valueForKey:@"status"] intValue] != 1)
        {
            
        }
        return dic;
    }
}
/*
-(NSMutableDictionary*)webServiceCalled : (NSString *)strWebServiceName : (NSMutableDictionary *)dicWebService
{
    @try {
        
        if(!appDelegate.isInternetAvailable)
        {
            [SVProgressHUD showErrorWithStatus:ERROR_NOINTERNET];
            return nil;
        }
        
        
        NSLog(@"=================== Web Service Call Start ===================");
        NSLog(@"URL : %@",[NSString stringWithFormat:@"%@%@",BASE_URL,strWebServiceName]);
        NSLog(@"BulkData : %@",[dicWebService description]);
        
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        NSString *strBulkData = [NSString stringWithFormat:@"bulk_data=[%@]",[dicWebService JSONRepresentation]];
        strBulkData = [strBulkData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSData* requestData = [NSData dataWithBytes:[strBulkData UTF8String] length:[strBulkData length]];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL,strWebServiceName]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr]];
        [request setHTTPMethod:@"POST"];
        [request addValue:@"application/x-www-form-urlencoded " forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:data];
        [request setTimeoutInterval:100];

        
//        NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
//        [req setHTTPMethod:@"POST"];
//        [req setTimeoutInterval:DEFAULTTIMEOUT];
//        [req setValue:[NSString stringWithFormat:@"%d", requestData.length] forHTTPHeaderField:@"Content-Length"];
//        [req setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//        [req setHTTPBody:requestData];
        
        NSError *err = nil;
        NSHTTPURLResponse *res = nil;
        NSData *retData = [NSURLConnection sendSynchronousRequest:req returningResponse:&res error:&err];
        
        strBulkData = nil;
        requestData = nil;
        url = nil;
        req = nil;
        res = nil;
        
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        if (err)
        {
            [SVProgressHUD showErrorWithStatus:ERROR_WEBSERVICES];
            return nil;
        }
        else
        {
            NSString *returnString = [[NSString alloc] initWithData:retData encoding:NSUTF8StringEncoding];
            NSMutableDictionary *dic = [returnString JSONValue];
            NSLog(@"Response : %@",[dic description]);
            NSLog(@"=================== Web Service Call End ===================");
            if([[dic valueForKey:@"status"] intValue] != 1)
            {
                
            }
            return dic;
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
*/
#pragma mark - Service
#pragma mark -----------------

+(void)callWebSevice :(NSObject *)delegate webServiceName : (NSString *)strWebServiceName parameterDic : (NSMutableDictionary *)dicData
{
    
    @try {
        NSLog(@"%s",__FUNCTION__);
        
        if (appDelegate.isInternetAvailable) {
            [SVProgressHUD showWithStatus:MSG_PLEASEWAIT];
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            
            
//            NSString *strJSONString;
//            if ([dicData count] > 0)
//            {
//                
//               strJSONString = [NSString stringWithFormat:@"bulk_data=[%@]",[dicData JSONRepresentation]];
//            }
            
            NSString *strURL = [NSString stringWithFormat:@"%@%@",WEBSERVICE_URL,strWebServiceName];
            
            strURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

            NSURL* url = [[NSURL alloc] initWithString:strURL];
            
            
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
            
            
            [request addRequestHeader:@"Content-Type" value:@"application/JSON"];
            [request setDelegate:delegate];
            [request setRequestMethod:@"POST"];
            [request setTimeOutSeconds:TIME_OUT];

            NSArray *ary = [dicData allKeys];
            for(NSString *strKey in ary)
            {
                if([strKey isEqualToString:KEY_FILE])
                    [request setFile:[dicData valueForKey:strKey] forKey:KEY_FILE];
                else
                    [request setPostValue:[dicData valueForKey:strKey] forKey:strKey];
            }

            request.shouldAttemptPersistentConnection = NO;
            [request setDidFinishSelector:@selector(uploadRequestFinished:)];
            [request setDidFailSelector:@selector(uploadRequestFailed:)];
            [request startAsynchronous];
            
        }
        else{
        
            [SVProgressHUD dismiss];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            UIAlertView *alertNoInternet = [[UIAlertView alloc] initWithTitle:APPLICATIONNAME message:MSG_NO_INTERNET delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertNoInternet show];
        
        }
        
    }
    @catch (NSException *exception) {
        [SVProgressHUD dismiss];
        NSLog(@"%s",__FUNCTION__);
        
    }
    
    
}

+(void)uploadImageToServer :(UIImage *)image{
    
    NSString *strURL = [NSString stringWithFormat:@""];
    NSURL* url = [[NSURL alloc] initWithString:strURL];
    
    UIImage *iim = [UIImage imageNamed:@"BuyerBeshboardCar2.png"];
    NSData *imageData = UIImagePNGRepresentation(iim);
    
    NSString *str1 = [imageData base64Encoding];
    
    NSMutableArray *ary = [[NSMutableArray alloc] init];
    NSMutableDictionary *dict;

    for(int i=4;i<6;i++)
    {
        dict = nil;
        dict =[[NSMutableDictionary alloc] init];
        [dict setObject:[NSString stringWithFormat:@"%d.png",i] forKey:@"name"];
        [dict setObject:str1 forKey:@"image"];
        [ary addObject:dict];

    }
    
    NSDictionary *dict1 = [NSDictionary dictionaryWithObjectsAndKeys:ary,@"imgArr",nil];
    
    SBJsonWriter *writer = [SBJsonWriter new];
    NSString * jsonString = [writer stringWithObject:dict1];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request setPostValue:jsonString forKey:@"addvehiimage"];
    [request addRequestHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    
    [request setDelegate:appDelegate];
    [request setTimeOutSeconds:600];
    request.shouldAttemptPersistentConnection = NO;
    [request setDidFinishSelector:@selector(uploadRequestFinished:)];
    [request setDidFailSelector:@selector(uploadRequestFailed:)];
    [request startAsynchronous];

}

-(void)uploadRequestFinished:(ASIHTTPRequest *)request
{

}
- (void)uploadRequestFailed:(ASIHTTPRequest *)request
{
    
}


/*
 {
 NSString *strBulkData = [NSString stringWithFormat:@"bulk_data=[%@]",[dicWebService JSONRepresentation]];
 strBulkData = [strBulkData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
 //        NSString* str = [NSString stringWithUTF8String:[strBulkData cStringUsingEncoding:NSUTF8StringEncoding]];
 
 NSData* requestData = [NSData dataWithBytes:[strBulkData UTF8String] length:[strBulkData length]];
 
 //        NSData *postData = [str dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
 
 NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",webServiceURL,strWebServiceName]];
 NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
 
 //        NSMutableURLRequest* req = [NSMutableURLRequest requestWithURL:url
 //                                                               cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
 //                                                           timeoutInterval:180.0];
 
 [req setHTTPMethod:@"POST"];
 [req setTimeoutInterval:6000];
 [req setValue:[NSString stringWithFormat:@"%d", requestData.length] forHTTPHeaderField:@"Content-Length"];
 [req setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
 [req setHTTPBody:requestData];
 
 NSError *err = nil;
 NSHTTPURLResponse *res = nil;
 NSData *retData = [NSURLConnection sendSynchronousRequest:req returningResponse:&res error:&err];
 
 strBulkData = nil;
 requestData = nil;
 url = nil;
 req = nil;
 res = nil;
 if (err)
 {
 //handle error]
 return nil;
 }
 else
 {
 //handle response and returning data
 NSString *returnString = [[NSString alloc] initWithData:retData encoding:NSUTF8StringEncoding];
 NSMutableDictionary *dic = [returnString JSONValue];
 //            NSLog(@"%@",dic.description);
 return dic;
 }
 
 }
 */
@end
