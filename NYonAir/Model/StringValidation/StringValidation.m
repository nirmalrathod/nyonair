//
//  StringValidation.m
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 8/8/13.
//  Copyright (c) 2013 Nirmalsinh Rathod. All rights reserved.
//

#import "StringValidation.h"

@implementation StringValidation

#pragma mark Remove Special Character:

-(NSString *) formatIdentificationNumber:(NSString *)string
{
	NSCharacterSet * invalidNumberSet = [NSCharacterSet characterSetWithCharactersInString:@"\n_!@#$%^&*()[]{}'\\.,<>:;|\\/?+=\t~`-"];
    
    NSString  * result  = @"";
    NSScanner * scanner = [NSScanner scannerWithString:string];
    NSString  * scannerResult;
    
    [scanner setCharactersToBeSkipped:nil];
    
    while (![scanner isAtEnd])
    {
        if([scanner scanUpToCharactersFromSet:invalidNumberSet intoString:&scannerResult])
        {
            result = [result stringByAppendingString:scannerResult];
        }
        else
        {
            if(![scanner isAtEnd])
            {
                [scanner setScanLocation:[scanner scanLocation]+1];
            }
        }
    }
    
    invalidNumberSet = nil;
    scanner = nil;
    scannerResult = nil;
    return result;
}

#pragma mark Remove Space Character:

-(NSString *) removeSpace:(NSString *)string
{
	NSCharacterSet * invalidNumberSet = [NSCharacterSet characterSetWithCharactersInString:@" "];
    
    NSString  * result  = @"";
    NSScanner * scanner = [NSScanner scannerWithString:string];
    NSString  * scannerResult;
    
    [scanner setCharactersToBeSkipped:nil];
    
    while (![scanner isAtEnd])
    {
        if([scanner scanUpToCharactersFromSet:invalidNumberSet intoString:&scannerResult])
        {
            result = [result stringByAppendingString:scannerResult];
        }
        else
        {
            if(![scanner isAtEnd])
            {
                [scanner setScanLocation:[scanner scanLocation]+1];
            }
        }
    }
    
    invalidNumberSet = nil;
    scanner = nil;
    scannerResult = nil;
    return result;
}

#pragma mark Only A-Z:
-(BOOL)inputStringOnlyAZ :(NSString *)string
{
    NSString *expression2 = @"([a-z])";
    NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:expression2
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    NSUInteger numberOfMatches = [regex2 numberOfMatchesInString: string
                                                         options:0
                                                           range:NSMakeRange(0, [string length])];
	if(numberOfMatches == 0)
		return FALSE;
	else
		return TRUE;
    
	return FALSE;
    
}

#pragma mark - Only 0-9
-(BOOL)inputStringOnly09 :(NSString *)string
{
    NSString *expression2 = @"([0-9])";
    NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:expression2
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    NSUInteger numberOfMatches = [regex2 numberOfMatchesInString: string
                                                         options:0
                                                           range:NSMakeRange(0, [string length])];
	if(numberOfMatches == 0)
		return FALSE;
	else
		return TRUE;
    
	return FALSE;
    
}
#pragma mark - Only a-z
-(BOOL)inputStringOnlyaz :(NSString *)string
{
    NSString *expression2 = @"([a-z])";
    NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:expression2
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    NSUInteger numberOfMatches = [regex2 numberOfMatchesInString: string
                                                         options:0
                                                           range:NSMakeRange(0, [string length])];
	if(numberOfMatches == 0)
		return FALSE;
	else
		return TRUE;
    
	return FALSE;
    
}

#pragma mark Email Validation:
+(BOOL) validateEmail: (NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}
#pragma mark Height From the String
+(float)heightFromString : (NSString *)strText : (UIFont *)fontText
{
    
    
    CGSize boundingSize = CGSizeMake(135, CGFLOAT_MAX);
    CGSize requiredSize = [strText sizeWithFont:fontText
                               constrainedToSize:boundingSize
                                   lineBreakMode:UILineBreakModeWordWrap];
    CGFloat requiredHeight = requiredSize.height;
    return requiredHeight;
    
//    CGSize stringSize = [strText sizeWithFont:fontText constrainedToSize:CGSizeMake(320, 9999)];
//    return stringSize.height;// + 40;
}
#pragma mark Remove All Special Character
+(NSString *)replaceCharacter : (NSString *)str
{
    str = [str stringByReplacingOccurrencesOfString:@"'" withString:singleQuate];
    str = [str stringByReplacingOccurrencesOfString:@"\"" withString:doubleQuate];
    return str;

}
+(NSString *)replaceInverseCharacter : (NSString *)str
{
    str = [str stringByReplacingOccurrencesOfString:singleQuate withString:@"'"];
    str = [str stringByReplacingOccurrencesOfString:doubleQuate withString:@"\""];
    return str;
    
}

+(NSString*) encodeToPercentEscapeString:(NSString *)string
{
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

+(NSString*) decodeFromPercentEscapeString:(NSString *)string
{
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                            (CFStringRef) string,
                                                            CFSTR(""),
                                                            kCFStringEncodingUTF8));
}


@end
