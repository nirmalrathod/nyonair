//
//  StringValidation.h
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 8/8/13.
//  Copyright (c) 2013 Nirmalsinh Rathod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringValidation : NSObject
-(NSString *) formatIdentificationNumber:(NSString *)string;
-(NSString *) removeSpace:(NSString *)string;
-(BOOL)inputStringOnly09 :(NSString *)string;
-(BOOL)inputStringOnlyAZ :(NSString *)string;
-(BOOL)inputStringOnlyaz :(NSString *)string;
+(BOOL) validateEmail: (NSString *) email;
+(float)heightFromString : (NSString *)strText : (UIFont *)fontText;
+(NSString*) encodeToPercentEscapeString:(NSString *)string;
+(NSString*) decodeFromPercentEscapeString:(NSString *)string;
+(NSString *)replaceCharacter : (NSString *)str;
+(NSString *)replaceInverseCharacter : (NSString *)str;
@end
