//
//  Constant.h
//  ClubGuru
//
//  Created by Nirmalsinh Rathod on 8/8/13.
//  Copyright (c) 2013 Nirmalsinh Rathod. All rights reserved.
//

#ifndef ClubGuru_Contant_h
#define ClubGuru_Contant_h

#define singleQuate @"_quat"
#define doubleQuate @"_dquat"

#define NSLog if(1) NSLog  // 0 - There is no log in conlose, 1 - enable.

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define TIME_OUT 600
#define ANIMATION_TIME  0.7f

#define keyboardHeightPortrait_iPhone 216
#define keyboardHeightLandscape_iPhone 162
#define keyboardHeightPortrait_iPad 264
#define keyboardHeightLandscape_iPad 352


#define BASEURL @""

#define APPLICATIONNAME @"NYonAir"
#define MSG_NO_INTERNET @"There is no internet available."

#define FONTNAME @"HelveticaNeue"
#define FONTNAMEBOLD @"HelveticaNeue-Bold"

#define NOPLACE @"No Places"

#define MSG_PLEASEWAIT @"Please Wait..."
#define MESSAGE_SERVER_ISSUE @"There is something wrong with server. Please try again later."


#define WEBSERVICE_URL @"http://patdayimages.com/sales/api/"

#define kTutorialPointProductID @"com.CreoleStudios.NYonAir.testProduct"



#define CACHE_DIRECTORY [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]


// Tag for EditView
#define TAG_BOLD @"textBold"
#define TAG_FONT @"textFont"
#define TAG_ALIGNMENT @"textAlignment"
#define TAG_TEXT @"textTitle"
#define TAG_TEXT_COLOR @"textColor"

#define TEXTALLIGNMENT_CENTER @"centerTextAlignment"
#define TEXTALLIGNMENT_RIGHT @"rightTextAlignment"
#define TEXTALLIGNMENT_LEFT @"leftTextAlignment"

// End

// Notificaiton for FlipView
#define NOTIFICAITON_BUYFLIP @"flipBuyView"
#define NOTIFICAITON_BUYCLICKED @"albumBuy"
// End

// Default Images
#define IMAGE_BACK [UIImage imageNamed:@"back.png"]
#define IMAGE_MENU [UIImage imageNamed:@"menu.png"]


// End

// Pages To Load
#define WEBPAGE_GALLERY @"http://gallery.nyonair.com/"
#define WEBPAGE_FACEBOOK @"https://www.facebook.com/NYonAir"
// End

// Title Name
#define TITLE_PHOTO @"Photos"
#define TITLE_ECARD @"E-Card"
#define TITLE_NEWS @"News"
#define TITLE_CONTACT @"Contact"
#define TITLE_SUBSCRIPTION @"Subscription"
#define TITLE_PROFILE @"Profile"
#define TITLE_UPDATEPROFILE @"Update Profile"
#define TITLE_LOGOUT @"Logout"
#define TITLE_FACEBOOKLIKE @"Facebook"
#define TITLE_VISITUS @"Visit Us"

// End

// Button name
#define BUTTON_OK @"OK"
#define BUTTON_CANCEL @"Cancel"

#define BUTTON_BUY @"Buy"
#define BUTTON_NO @"No"

// End

// Sharing
#define NOTIFICAITON_INSTAGRAMFOLLOW @"instagramfollow"
#define MSG_INSTAGRAMFOLLOW_SUCCESS @"Your request send to user."
#define MSG_INSTAGRAMFOLLOW_FAILURE @"There is someting worng with request."
#define INSTAGRAM_USERID @"6742577"
// End


// e-Card Label

#define MSG_SELECTTEMPLATE @"Select any template"
#define MSG_TYPEMESSAGE @"Edit the image to add your message."
#define MSG_SENDEMAIL @"Send the card via email"


#define MSG_TITLE_NO_TEXT @"No message added."
#define MSG_TITLE_TEXT @"Please add a message of your choice on the card."
// End

// --------------------> Alert Message <--------------------

#define MSG_PLEASEWAIT @"Please Wait..."
// Login/Registration Screen
#define MSG_ENTEREMAIL @"Please enter your email address."
#define MSG_INVALIDEMAIL @"Please enter a valid email address."
#define MSG_ENTERPASSWORD @"Please enter your account's password."
#define MSG_ENTERPASSWORDNEW @"Please enter your account's new password."
#define MSG_ENTERPASSWORDRETYPE @"Please enter your account's retype password."
#define MSG_PASSWORDNOTMATCH @"Your new password and retype password does not match."

#define MSG_ENTERFIRSTNAME @"Please enter your first name."
#define MSG_ENTERLASTNAME @"Please enter your last name."

// eCard
#define MSG_ECARDSUBMIT @"Your e-card has been emailed successfully."
// End Screen

// Detail Screen
#define MSG_THANKYOUMESSAGE @"Thank you for purchasing the album. The high resolution photos will be emailed to you shortly."
#define MSG_THANKYOUTITLE   @"Thank you"

#define MSG_BUYSUBSCRIPTION @"Buy 1-year subscription and download all the albums for free."
#define MSG_BUYSUBSCRIPTIONTITLE   @"Buy"


#define MSG_SERVERERROR @"There is something worng, Please try again later."
// End Screen

// Menu Screen
#define MSG_LOGOUT @"Are you sure you want to logout?"
#define MES_NOCHANGES @"No changes were made."
// End Screen

// --------------------> End Alert Message <--------------------


// Web-Service Name

// User Section
#define WS_LOGIN @"customer/login"
#define WS_REGISTER @"customer/register"
#define WS_FORGOTPASSWORD @"customer/forgotpassword"
#define WS_CUSTOMERDETAILS @"customer/customerdetails"
#define WS_CHANGEPASSWORD @"customer/changepassword"
#define WS_UDPATEDUSERDETAIL @"customer/updatedetails"
// End

// Album Section
#define WS_ALBUMSLIST @"albums/albumlist"
#define WS_ALBUMSFAVORITE @"albums/albumfavorite"
#define WS_ALBUMSVIEW @"albums/albumview"
#define WS_ALBUMSDETAILS @"albums/details"
#define WS_ALBUMSFAVORITE @"albums/albumfavorite"
// End

// News
#define WS_NEWS @"news"
// End

// Ecard
#define WS_ECARD @"ecard/send"
// END

#define WS_SUBSCRIPTION @"customer/subscription"
// End Web-Service Name

// Key for all Web-Services

#define KEY_SUCCESS @"IsSuccess"
#define KEY_MSG @"Message"
#define KEY_DATA @"Data"

#define KEY_EMAIL @"email"
#define KEY_PASSWORD @"password"
#define KEY_FIRSTNAME @"first_name"
#define KEY_LASTNAME @"last_name"
#define KEY_CUSTOMERID @"customer_id"
#define KEY_OLDPASSWORD @"old_password"
#define KEY_NEWPASSWORD @"new_password"

#define KEY_OFFSET @"offset"
#define KEY_ALBUMID @"album_id"
#define KEY_ALBUMNAME @"album_name"
#define KEY_ALBUMPRICE @"album_price"
#define KEY_ALBUMCOVERIMAGE @"cover_image"
#define KEY_ALBUMFAVORITECOUNT @"album_favorite_count"
#define KEY_ALBUMVIEWCOUNT @"album_view_count"

#define KEY_FAVOROTECOUNT @"favorite_count"
#define KEY_VIEWCOUNT @"view_count"
#define KEY_IMAGEID @"image_id"
#define KEY_IMAGEURL @"image_url"

#define KEY_USERFLAG @"user_flag"
#define KEY_FAVORITEBYUSER @"fav_by_user"
#define KEY_VIEWBYUSER @"view_by_user"
#define KEY_COUNT @"count"
#define KEY_IMAGES @"images"
// End Key
#define KEY_FLAGE @"flag"
#define KEY_NEWSDESCRIPTION @"description"
#define KEY_NEWSID @"news_id"
#define KEY_NEWSTITLE @"title"
#define KEY_NEWSIMAGE @"news_image"

#define KEY_CUSTOMEREMAIL @"customer_email"
#define KEY_FILE @"file"
#endif
