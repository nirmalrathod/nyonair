//
//  AppDelegate.m
//  NYonAir
//
//  Created by Nirmalsinh Rathod on 4/11/14.
//  Copyright (c) 2014 Nirmalsinh Rathod. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "LoginViewController.h"
#import "HomeViewController.h"
@implementation AppDelegate
@synthesize isInternetAvailable,isiPhone5;
@synthesize objNavigation,lastController;
@synthesize strCustomerID,isDetailChanged;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
//    NSString *str = [self convertIntoKserias:1234];
//    NSLog(@"%@",str);
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    
    // Observe the kNetworkReachabilityChangedNotification. When that notification is posted, the
    // method "reachabilityChanged" will be called.
    isInternetAvailable = FALSE;
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    
    //Change the host name here to change the server your monitoring
	hostReach = [Reachability reachabilityWithHostName: @"www.apple.com"];
	[hostReach startNotifier];
	[self updateInterfaceWithReachability: hostReach];
    //End Reachability code
    
    // Here is the code to check wheather detected device is iPhone5 or not
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    if (screenSize.height > 480.0f)
    {
        self.isiPhone5 = TRUE;
    }
    else
    {
        self.isiPhone5 = FALSE;
    }
    // End
    
    isDetailChanged = FALSE;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    LoginViewController *objHome;
    
    if(isiPhone5)
        objHome = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    else
        objHome = [[LoginViewController alloc] initWithNibName:@"LoginViewController_iPhone4" bundle:nil];

    UINavigationController *objLoginNavigation = [[UINavigationController alloc] initWithRootViewController:objHome];

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"titlebar.png"]]];//[UIColor colorWithRed:198/255.0 green:23/255.0 blue:51/255.0 alpha:1.0]
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        [[UINavigationBar appearance] setBackIndicatorImage:IMAGE_BACK];
        [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:IMAGE_BACK];
    }
    else
    {
        //        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }

    self.window.rootViewController = objLoginNavigation;
    [self.window makeKeyAndVisible];

    return YES;
}
#pragma mark - Add Left Menu
-(void)addLeftMenu
{
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    HomeViewController *objHome = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    objNavigation = [[UINavigationController alloc] initWithRootViewController:objHome];

    objNavigation.navigationBar.translucent = NO;
    objNavigation.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    
    MenuViewController *objLeftMenu = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
    
    
    
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:objNavigation
                                                    leftMenuViewController:nil
                                                    rightMenuViewController:objLeftMenu];
    
    container.panMode = FALSE;
    self.window.rootViewController = container;
    [self.window makeKeyAndVisible];
    
    /*
     dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
     dispatch_async(queue, ^{
     
     });
     });

     */
    
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(void)openLoginView
{
    
}
#pragma mark - Download Image And Store into Local
-(void)storedImage : (NSString *)strImageURL
{
    @try {
        NSString *strImageName;
        NSArray *ary = [strImageURL componentsSeparatedByString:@"/"];
        strImageName = [ary lastObject];
        strImageName = [strImageName stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *imagePath = [CACHE_DIRECTORY stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",strImageName]];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL fileExists = [fileManager fileExistsAtPath:imagePath];
        
        if(fileExists == FALSE)
        {
            NSURL *url = [NSURL URLWithString:[strImageURL stringByAddingPercentEscapesUsingEncoding:
                                               NSASCIIStringEncoding]];
            NSData *imageData = [NSData dataWithContentsOfURL:url];
            [imageData writeToFile:imagePath atomically:YES];
        }
        
        ary = nil;
        strImageName = nil;
        
    }
    @catch (NSException *exception) {
        NSLog(@"%s ==> %@",__FUNCTION__,exception.description);
    }
}
#pragma mark - Convert into K
-(NSString *)convertIntoKserias :(int)convertNumber
{
    if(convertNumber < 1000)
        return [NSString stringWithFormat:@"%d",convertNumber];
    else
        return [NSString stringWithFormat:@"%.1fk",(float)convertNumber/1000];
}
#pragma mark - Reachability
//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateInterfaceWithReachability: curReach];
}
- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    if(curReach == hostReach)
	{
        // NetworkStatus netStatus = [curReach currentReachabilityStatus];
        BOOL connectionRequired= [curReach connectionRequired];
        
        NSString* baseLabel=  @"";
        if(connectionRequired)
        {
            self.isInternetAvailable = FALSE;
            baseLabel=  @"Cellular data network is available.\n  Internet traffic will be routed through it after a connection is established.";
        }
        else
        {
            baseLabel=  @"Cellular data network is active.\n  Internet traffic will be routed through it.";
            self.isInternetAvailable = TRUE;
        }
        NSLog(@"%@",baseLabel);
    }
    //	if(curReach == internetReach)
    //	{
    //		[self configureTextField: internetConnectionStatusField imageView: internetConnectionIcon reachability: curReach];
    //	}
    //	if(curReach == wifiReach)
    //	{
    //		[self configureTextField: localWiFiConnectionStatusField imageView: localWiFiConnectionIcon reachability: curReach];
    //	}
	
}
#pragma mark - Begin and End Ignore Event
-(void)beginIgnoreEvent
{
    @try
    {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}
-(void)endIgnoreEvent
{
    @try
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception :  %s",__FUNCTION__);
    }
}

#pragma mark - InApp Purchase Method
-(void)fetchAvailableProducts
{
    NSSet *productIdentifiers = [NSSet
                                 setWithObjects:kTutorialPointProductID,nil];
    productsRequest = [[SKProductsRequest alloc]
                       initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}
- (void)purchaseMyProduct:(SKProduct*)product{
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"Purchases are disabled in your device" message:nil delegate:
                                  self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
    }
}
-(IBAction)purchase:(id)sender{
    [self purchaseMyProduct:[validProducts objectAtIndex:0]];
}

#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:kTutorialPointProductID]) {
                    NSLog(@"Purchased ");
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    [alertView show];
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                break;
            default:
                break;
        }
    }
}

-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response
{
    SKProduct *validProduct = nil;
    int count = [response.products count];
    if (count>0) {
        validProducts = response.products;
        validProduct = [response.products objectAtIndex:0];
        if ([validProduct.productIdentifier
             isEqualToString:kTutorialPointProductID]) {
            
            NSLog(@"%@",[NSString stringWithFormat:
                         @"Product Title: %@",validProduct.localizedTitle]);
            NSLog(@"%@",[NSString stringWithFormat:
                         @"Product Desc: %@",validProduct.localizedDescription]);
            NSLog(@"%@",[NSString stringWithFormat:
                         @"Product Price: %@",validProduct.price]);
        }
    } else {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Not Available"
                            message:@"No products to purchase"
                            delegate:nil
                            cancelButtonTitle:nil
                            otherButtonTitles:@"Ok", nil];
        [tmp show];
    }    
}



@end
